from arches.settings_local import *
import logging
import os

use_google_cloud_storage = os.environ.get("USE_GOOGLE_CLOUD_STORAGE", False)

ALLOWED_HOSTS = ['localhost', '*'] # get_env_variable("DOMAIN_NAMES").split()

CELERY_BROKER_URL = get_env_variable("CELERY_BROKER_URL")
MOBILE_OAUTH_CLIENT_ID = get_optional_env_variable("MOBILE_OAUTH_CLIENT_ID")
STATIC_URL = get_optional_env_variable("STATIC_URL") or "/media/"
COMPRESS_OFFLINE = get_optional_env_variable("COMPRESS_OFFLINE")
COMPRESS_OFFLINE = COMPRESS_OFFLINE and COMPRESS_OFFLINE.lower() == "true"
COMPRESS_ENABLED = True

if use_google_cloud_storage:
    from google.oauth2 import service_account
    DEFAULT_FILE_STORAGE = 'storages.backends.gcloud.GoogleCloudStorage'
    GS_BUCKET_NAME = get_env_variable("GS_BUCKET_NAME")
    GS_CREDENTIALS = service_account.Credentials.from_service_account_file(
        get_env_variable("GS_CREDENTIALS_FILE")
    )
    S3_URL = get_env_variable("GS_MEDIA_URL")
    MEDIA_URL = S3_URL
else:
    DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
    AWS_STORAGE_BUCKET_NAME = get_env_variable("AWS_STORAGE_BUCKET_NAME")
    AWS_ACCESS_KEY_ID = get_env_variable("AWS_ACCESS_KEY_ID")
    AWS_SECRET_ACCESS_KEY = get_env_variable("AWS_SECRET_ACCESS_KEY")
    AWS_S3_SIGNATURE_VERSION = 's3v4'
    AWS_S3_REGION_NAME = 'us-east-1'
    S3_URL = get_env_variable("S3_URL")
    MEDIA_URL = S3_URL
    AWS_S3_ENDPOINT_URL = get_env_variable("AWS_S3_ENDPOINT_URL")
