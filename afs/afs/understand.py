from django.contrib.auth.models import User, Group
from django.contrib.auth.hashers import make_password
import uuid
from colorama import Fore, Back, Style
import logging
import csv
import os
import uuid
from tabulate import tabulate
import sys
import django
from pathlib import Path
from arches.app.models.graph import Graph
from afs.resource_model_wrappers import get_well_known_resource_model_by_class_name, get_well_known_resource_model_by_graph_id
from afs.resource_model_hooks import project_related_model
from afs.views.register_as_researcher import RegisterAsResearcherView

def graph_all():
    from arches.app.models.graph import Graph

    table = []
    for graph in Graph.objects.all():
        table.append([graph.name, graph.subtitle, graph.pk])
    print(tabulate(table))

def describe_resource(resource):
    wkrm = get_well_known_resource_model_by_graph_id(resource.graph_id)
    if not wkrm:
        raise NotImplementedError(f"The graphid for {resource} is not well known")
    print(wkrm.from_resource(resource).describe())

def graph_one(graph, nodegroups):
    from arches.app.models.graph import Graph
    import asciitree
    from collections import OrderedDict as odict

    try:
        graphid = uuid.UUID(graph)
        graph = Graph.objects.get(pk=graphid)
    except ValueError:
        graph = Graph.objects.get(name=graph)
        graphid = graph.pk

    tree = graph.get_tree()
    def _format_node(node):
        return f"{Fore.GREEN}{node.name}{Fore.RESET} {Style.DIM}({node.pk}){Style.RESET_ALL} {node.nodegroup_id}"
    def _to_atree(ent):
        return odict([
            (_format_node(child['node']), _to_atree(child))
            for child in ent['children']
        ])
    atree = {f"{graph.name} ({graph.pk})": _to_atree(tree)}
    tr = asciitree.LeftAligned()
    print(tr(atree))

def load_resources_from_csv(resource_model, source):
    wkrm = get_well_known_resource_model_by_class_name(resource_model)
    columns = set()
    with open(source, newline="") as csvfile:
        reader = csv.DictReader(csvfile, delimiter=",", quotechar='"')
        table = [[str(wkrm.__name__), ""] + list(reader.fieldnames)]
        key = reader.fieldnames[0]
        for row in reader:
            for col in row:
                if col.endswith("*"):
                    row[col] = row[col].split(",")
            row = {col if col[-1] != "*" else col[:-1]: row[col] for col in row}
            ri = wkrm.where(**{key: row[key]})
            if ri:
                ri = ri[0]
                ri.update(row)
                ri.save()
                sym = "o"
            else:
                ri = wkrm.create(**row)
                sym = "+"
            ri.save()

            relationships = {col.split(".", 1)[0] for col in reader.fieldnames if "." in col}
            for relation in relationships:
                relateds = getattr(ri, relation)
                for related in relateds:
                    related.append()
            ri.save()

            project_related_model(wkrm, instance=ri)
            table.append(
                ["+", str(ri)] + [row[field if field[-1] != "*" else field[:-1]] for field in reader.fieldnames]
            )
    print(tabulate(table))

def load_users_from_csv(source):
    required_groups = [
        Group.objects.get(name="Crowdsource Editor")
    ]
    with open(source, newline="") as csvfile:
        reader = csv.DictReader(csvfile, delimiter=",", quotechar='"')
        table = [[""] + list(reader.fieldnames)]
        for row in reader:
            row["username"] = str(uuid.uuid4())
            password = row["password"]
            row["password"] = make_password(password)
            local_row = {k: v for k, v in row.items() if not k.startswith("person.")}
            person_row = {k.replace("person.", ""): v for k, v in row.items() if k.startswith("person.")}
            try:
                user = User.objects.get(email=row["email"])
            except User.DoesNotExist:
                user = User.objects.create(**local_row)
                sym = "+"
            else:
                user = User.objects.filter(email=row["email"]).update(**local_row)
                user = User.objects.get(email=row["email"])
                sym = "o"

            for group in required_groups:
                user.groups.add(group)
            user.save()

            if person_row:
                rr = RegisterAsResearcherView()
                class Req:
                    def __init__(self, user, **kwargs):
                        self.POST = kwargs
                        self.user = user
                rr.post(Req(user=user, **person_row))

            row["password"] = password
            table.append(
                [sym] + [row[field] for field in reader.fieldnames]
            )
    print(tabulate(table))
