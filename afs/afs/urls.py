from django.conf.urls import include, url
from django.conf import settings
from django.conf.urls.static import static
from arches.app.views.plugin import PluginView
from afs.views.environment_search import EnvironmentSearchView
from afs.views.environments_in_set import EnvironmentSetView 
from afs.views.access_rights import AccessRights 
from afs.views.update_resource_list import UpdateResourceListView
from afs.views.register_as_researcher import RegisterAsResearcherView
from afs.views.available_workflows import AvailablePluginsView
from afs.views.digital_resources_by_object_parts import DigitalResourcesByObjectParts

uuid_regex = settings.UUID_REGEX

urlpatterns = [
    url(r"^", include("arches.urls")),
    url(r"^environment_search_results", EnvironmentSearchView.as_view(), name="environment_search_results"),
    url(r"^environments-in-set", EnvironmentSetView.as_view(), name="environments_set"),
    url(r"^access_rights", AccessRights.as_view(), name="access_rights"),
    url(
        r"^digital-resources-by-object-parts/(?P<resourceid>%s)$" % uuid_regex,
        DigitalResourcesByObjectParts.as_view(),
        name="digital-resources-by-object-parts",
    ),
    url(r"^updateresourcelist", UpdateResourceListView.as_view(), name="updateresourcelist"),
    url(r"^register-as-researcher", RegisterAsResearcherView.as_view(), name="registerasresearcher"),
    url(r"^api/available-plugins", AvailablePluginsView.as_view(), name="availableplugins"),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
