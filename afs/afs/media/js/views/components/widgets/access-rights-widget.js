define([
    'knockout',
    'underscore', 
    'viewmodels/widget'
], function (ko, _, WidgetViewModel) {
    return ko.components.register('access-rights-widget', {
        viewModel: function(params) {
            WidgetViewModel.apply(this, [params]);
            var self = this

            this.permissions = ko.observable("readOnly");
            this.value("readOnly")
            this.permissions.subscribe(function(val){
                self.value(val)
            })
        },
        template: { require: 'text!templates/views/components/widgets/access-rights-widget.htm' }
    });
});