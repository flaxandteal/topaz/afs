define([
    'knockout',
    'views/components/workflows/summary-step',
], function(ko, SummaryStep) {

    function viewModel(params) {
        var self = this;

        SummaryStep.apply(this, [params]);

        this.projectResourceId = params.projectResourceId;
        this.relatedResources.subscribe(function(val){
            
            self.projectName = self.getResourceValue(val, ["resource_instance","displayname"]);
            self.relatedEnvResources = self.getResourceValue(val, ["related_resources"]);
       
            this.loading(false);
        }, this);
    }

    ko.components.register('project-collection-final-step', {
        viewModel: viewModel,
        template: { require: 'text!templates/views/components/workflows/project-collection-workflow/project-collection-final-step.htm' }
    });
    return viewModel;
});
