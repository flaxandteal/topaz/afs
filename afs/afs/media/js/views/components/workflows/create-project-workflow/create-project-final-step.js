define([
    'knockout',
    'views/components/workflows/summary-step',
], function(ko, SummaryStep) {

    function viewModel(params) {
        var self = this;
        SummaryStep.apply(this, [params]);

        this.resourceLoading = ko.observable(true);
        this.collectionLoading = ko.observable(true);

        this.collectionResourceId = params.resourceid;
        this.collectionData = ko.observableArray();
        this.collectionOfEnvs = ko.observableArray();

        this.getRelatedResources(this.collectionResourceId, this.collectionData);

        // todo tidy up there are no phys things anymore 
        this.collectionData.subscribe(function(val){
            var environmentGraphId = '9519cb4f-b25b-11e9-8c7b-a4d18cec433a';
            val["related_resources"].forEach(function(rr){
                if (rr.graph_id === environmentGraphId) {
                    self.collectionOfEnvs.push({
                        resourceid: rr.resourceinstanceid,
                        name: rr.displayname,
                    });
                }
            });
            this.collectionLoading(false);
            if (!this.resourceLoading()){
                this.loading(false);
            }
        }, this);

        this.resourceData.subscribe(function(val){
            console.log(val.resource)
            this.displayName = val['displayname'] || 'unnamed';
            this.reportVals = {
                projectName: {'name': 'Project Name', 'value': val.resource['Basic Info'][0]['Name']['@display_value']},
                projectStatement: {'name': 'Project Statement', 'value': val.resource['Description'][0]['Statement']['@display_value']},
                projectObjective: {'name': 'Project Objective', 'value': val.resource['Description'][0]['Objective']['@display_value']},
                projectInfluence: {'name': 'Project Influence', 'value': val.resource['Description'][0]['Influence']['@display_value']},
                // projectName: {'name': 'Project Name', 'value': this.getResourceValue(val.resource['Basic Info'][0]['Name'][0],['Name_content','@display_value'])},
                projectTimespan: {'name': 'Project Timespan', 'value': val.resource['Project start']['Date & Time']['@display_value']},
                projectTeam: {'name': 'Project Team', 'value': val.resource['Project start']['Group']['@display_value'] || "N/A"},
                projectLocation: {'name': 'Project Location', 'value': val.resource['Project start']['Project location']['@display_value'] || "N/A"},
                // projectTechnique: {'name': 'Technique', 'value': val.resource['Technique']['@display_value']|| "N/A"},
                collection: {'name': 'Related Collection', 'value': this.getResourceValue(val.resource['Used Set'], ['@display_value']) || "N/A"},
            };

            this.resourceLoading(false);
            if (!this.collectionLoading()){
                this.loading(false);
            }
        }, this);

    }

    ko.components.register('create-project-final-step', {
        viewModel: viewModel,
        template: { require: 'text!templates/views/components/workflows/create-project-workflow/create-project-final-step.htm' }
    });
    return viewModel;
});
