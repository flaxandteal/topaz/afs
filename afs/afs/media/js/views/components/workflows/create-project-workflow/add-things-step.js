define([
    'jquery',
    'underscore',
    'knockout',
    'knockout-mapping',
    'uuid',
    'arches',
    'bindings/select2-query',
    'views/components/search/paging-filter',
], function($, _, ko, koMapping, uuid, arches) {
    var getQueryObject = function() {
        var query = _.chain(decodeURIComponent(location.search).slice(1).split('&'))
            // Split each array item into [key, value]
            // ignore empty string if search is empty
            .map(function(item) {
                if (item) return item.split('=');
            })
            // Remove undefined in the case the search is empty
            .compact()
            // Turn [key, value] arrays into object parameters
            .object()
            // Return the value of the chain operation
            .value();
        return query;
    };
    
    function viewModel(params) {

        var self = this;

        _.extend(this, params.form);

        var limit = 7;
        this.projectResourceId = ko.observable();
        this.reportDataLoading = ko.observable(params.loading());

        if (params.projectResourceId){
            console.log(params.projectResourceId)
            console.log(params.resourceid)
            const projectResourceId = params.projectResourceId;
            this.projectResourceId(projectResourceId);
        } else if (params.resourceid){
            this.projectResourceId(params.resourceid);
        }
        console.log(params.projectResourceId, ko.unwrap(this.projectResourceId), 'ri')

        this.searchResults = {'timestamp': ko.observable()};
        this.targetResource = ko.observable();
        this.filters = {
            'paging-filter': ko.observable(),
            'search-results': ko.observable(),
        };
        this.termFilter = ko.observable();
        this.totalResults = ko.observable();
        this.query = ko.observable(getQueryObject());
        this.selectedTerm = ko.observable();
        this.targetResources = ko.observableArray([]);
        this.termOptions = [];
        this.value = ko.observableArray([]).extend({
            rateLimit: 100
        });
        this.startValue = ko.observableArray();
        this.selectedResources = ko.observableArray([]);
        this.addedValues = ko.observableArray();
        this.removedValues = ko.observableArray();
        this.existingEnvs = ko.observableArray();

        this.dirty = ko.pureComputed(function() {
            if (self.startValue() && self.value()){
                return !!(self.startValue().find(x => !self.value().includes(x))
                    || self.value().find(x => !self.startValue().includes(x)));
            } else {
                return false;
            }
        });

        this.dirty.subscribe(function(dirty) {
            params.dirty(dirty);
        });

        this.value.subscribe(function(a) {
            a.forEach(function(action) {
                if (action.status === 'added') {
                    $.ajax({
                        url: arches.urls.api_resources(ko.unwrap(action.value)),
                        data: {
                            format: 'json',
                            includetiles: 'false'
                        }
                    }).done(function(data) {
                        self.selectedResources.push(data);
                    });
                } else if (action.status === 'deleted') {
                    self.selectedResources().forEach(function(val) {
                        if (val.resourceinstanceid === ko.unwrap(action.value)) {
                            self.selectedResources.remove(val);
                        }
                    });
                }
            });
        }, null, "arrayChange");

        // loadExistingRelatedEnvs() loads all environments that the project has in its type of collection field
        const loadExistingRelatedEnvs = function(){
            const typeOfCollectionNodeId = 'ae16e448-033f-11ed-91f6-0242ac180008';
            // const typeOfCollectionNodeId = 'f68b39b2-026e-11ed-a74c-0242ac180008';
            const environmentsGraphId = '9519cb4f-b25b-11e9-8c7b-a4d18cec433a';
            $.ajax({
                url: arches.urls.environments_set,
                type: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    resourceid: ko.unwrap(self.projectResourceId),
                    nodegroupid: typeOfCollectionNodeId,
                    nodeid: typeOfCollectionNodeId
                }
            }).done(function(data) {
                data.items.forEach(item => {
                    console.log(item)
                    // if the item is an environment add it otherwise skip it
                    if (item._source.graph_id === environmentsGraphId) {
                        self.value.push(item._id);
                        self.existingEnvs.push(item._id);
                    }
                });
            });
        };

        this.initialize = function(){
            if (params.value()) {
                const cachedValue = ko.unwrap(params.value);
                if (cachedValue["value"]){
                    self.startValue(ko.unwrap(cachedValue["value"]));
                    self.startValue().forEach(function(val){
                        self.value.push(val);
                    });
                }
            } else if (params.action === "update") {
                loadExistingRelatedEnvs();
            }
        };

        //todo
        this.resetTile = function() {
            if (self.startValue()) {
                self.value.removeAll();
                self.startValue().forEach(function(val){
                    self.value.push(val);
                });
            }
        };

        params.form.reset = this.resetTile;

        // this makes it so that when you select an environment 
        // it's resourceid is being passed to self.value or removed if you so wish
        // one func is used for both it just asks if it's in the list or not 
        this.updateTileData = function(resourceid) {
            var val = self.value().find(function(item) {
                return ko.unwrap(item) === resourceid;
            });
            if (!!val) {
                // remove item, we don't want users to add the same item twice
                if (self.existingEnvs().includes(val)) {
                    self.removedValues.push(val);
                }
                self.value.remove(val);
            } else {
                self.addedValues.push(resourceid);
                self.value.push(resourceid);
            }
        };

        this.relatedEnvsToProjectChange = () => {
            let resourcesToUpdate = [];

            self.addedValues().map(function(value){
                resourcesToUpdate.push({
                    resourceid: value,
                    action: 'add',
                });
            });

            self.removedValues().map(function(value){
                resourcesToUpdate.push({
                    resourceid: value,
                    action: 'remove',
                });
            });

            console.log("resourcetoupdate on submit", resourcesToUpdate)

            // project, type of collections field
            // const memberOfSetNodegroupId = 'f68b39b2-026e-11ed-a74c-0242ac180008';
            const memberOfSetNodegroupId = 'ae16e448-033f-11ed-91f6-0242ac180008';
            console.log("res", self.projectResourceId())
            return $.ajax({
                url: arches.urls.root + 'updateresourcelist',
                type: 'POST',
                data: {
                    resourceid : ko.unwrap(self.projectResourceId),
                    nodegroupid : memberOfSetNodegroupId,
                    nodeid : memberOfSetNodegroupId,
                    transactionid : params.form.workflowId,
                    data: JSON.stringify(resourcesToUpdate)
                }
            });
        };

        this.submit = function() {
            self.complete(false);
            self.saving(true);
            if (params.action === "update") {
                params.form.lockExternalStep("select-project", true);
                self.relatedEnvsToProjectChange().fail((err) => {
                    const startValue = ko.unwrap(self.startValue);
                    self.value(startValue);
                }).always(() => {
                    // I think self.savedData() saves the data for the next step not sure
                    self.savedData(
                        {
                            value: ko.unwrap(self.value),
                            projectResourceId: ko.unwrap(self.projectResourceId),

                        }
                    );
                    self.saving(false);
                    self.complete(true);
                });
            } else {
                self.relatedEnvsToProjectChange().fail((err) => {
                    const startValue = ko.unwrap(self.startValue);
                    self.value(startValue);
                }).always(() => {
                    // I think self.savedData() saves the data for the next step not sure
                    self.savedData(
                        {
                            value: ko.unwrap(self.value),
                            projectResourceId: self.projectResourceId
                        }
                    );
                    self.saving(false);
                    self.complete(true);
                });
            }
        };

        params.form.save = self.submit;
        params.form.onSaveSuccess = function() {};

        this.targetResourceSelectConfig = {
            value: self.selectedTerm,
            placeholder: 'find an environment: enter an environment name or environment number - this could be a physical location, or area of context',
            minimumInputLength: 0,
            clickBubble: true,
            multiple: false,
            closeOnSlect: false,
            allowClear: true,
            ajax: {
                url: arches.urls.search_terms,
                dataType: 'json',
                quietMillis: 250,
                data: function(term, page) {
                    var data = {
                        start: (page - 1) * limit,
                        // eslint-disable-next-line camelcase
                        page_limit: limit,
                        q: term
                    };
                    return data;
                },
                results: function(data, page) {
                    const value = window.document.getElementsByClassName('select2-input')[0].value;
                    const results = data.terms;
                    results.unshift({
                        type: 'string',
                        context: '',
                        context_label: 'Search Term',
                        id: value,
                        text: value,
                        value: value
                    });
                    self.termOptions = results;

                    const filteredResults = results.filter(function(result){
                        return (result.context_label.includes("Environment") || result.context_label.includes("Search Term")) && 
                            !(result.text.includes("[Sample") || result.text.includes("[Analysis") || result.text.includes("[Region"));
                    });
                    return {
                        results: filteredResults,
                        more: data.count >= (page * limit)
                    };
                }
            },
            id: function(item) {
                return item.id;
            },
            formatResult: function(item) {
                if (item.context_label === 'Search Term') {
                    return `<strong><u>${item.text}</u></strong>`;
                }
                return item.text;
            },
            formatSelection: function(item) {
                return item.text;
            },
            clear: function() {
                self.selectedTerm();
            },
            isEmpty: ko.computed(function() {
                return self.selectedTerm() === '' || !self.selectedTerm();
            }, this),
            initSelection: function() {
                return;
            }
        };
        
        var getResultData = function(termFilter, pagingFilter) {
            var filters = {};
            // let's empty our termFilters
            _.each(self.filters, function(_value, key) {
                if (key !== 'paging-filter' && key !== 'search-results') {
                    delete self.filters[key];
                }
            });
            // b9c1ced7-b497-11e9-a4da-a4d18cec433a concept-list language
            // 87d0e63c-27d2-11ed-90be-0242ac120008 concept-list instrument
            // b9c1ced7-b497-11e9-a4da-a4d18cec433a concept-list material
            // 8ddfe3ab-b31d-11e9-aff0-a4d18cec433a concept-list og
            const advancedSearchFilter = [
                {
                    "op":"and",
                    "b9c1ced7-b497-11e9-a4da-a4d18cec433a":{
                        "op":"!",
                        "val":"31d97bdd-f10f-4a26-958c-69cb5ab69af1"
                    }
                },
                {
                    "op":"and",
                    "b9c1ced7-b497-11e9-a4da-a4d18cec433a":{
                        "op":"!",
                        "val":"7375a6fb-0bfb-4bcf-81a3-6180cdd26123"
                    }
                },
                {
                    "op":"and",
                    "b9c1ced7-b497-11e9-a4da-a4d18cec433a":{
                        "op":"!",
                        "val":"77d8cf19-ce9c-4e0a-bde1-9148d870e11c"
                    }
                },
                {
                    "op":"or",
                    "b9c1ced7-b497-11e9-a4da-a4d18cec433a":{
                        "op":"null",
                        "val":""
                    }
                }
            ];
            filters["advanced-search"] = JSON.stringify(advancedSearchFilter);

            if (termFilter) {
                termFilter['inverted'] = false;
                filters["term-filter"] = JSON.stringify([termFilter]);
            } 

            if (pagingFilter) {
                filters['paging-filter'] = pagingFilter;
                self.filters['paging-filter'](pagingFilter);
            } else {
                filters['paging-filter'] = 1;
            }

            self.reportDataLoading(true);

            const setUpReports = function() {
                const filterParams = Object.entries(filters).map(([key, val]) => `${key}=${val}`).join('&');
                fetch(arches.urls.environment_search_results + '?' + filterParams)
                    .then(response => response.json())
                    .then(data => {
                        _.each(self.searchResults, function(_value, key) {
                            if (key !== 'timestamp') {
                                delete self.searchResults[key];
                            }
                        });
                        _.each(data, function(value, key) {
                            if (key !== 'timestamp') {
                                self.searchResults[key] = value;
                            }
                        });
                        self.searchResults.timestamp(data.timestamp);
        
                        self.totalResults(data['total_results']);
                        var resources = data['results']['hits']['hits'].map(source => {
                            source._source.tiles.forEach((tile) => {
                                if (tile.data['22c15cfa-b498-11e9-b5e3-a4d18cec433a'] && 
                                    tile.data['22c15cfa-b498-11e9-b5e3-a4d18cec433a'].length &&
                                    tile.data['22c15cfa-b498-11e9-b5e3-a4d18cec433a'][0] === '26094e9c-2702-4963-adee-19ad118f0f5a') {
                                    source.identifier = tile.data['22c169b5-b498-11e9-bdad-a4d18cec433a'];
                                }
                            });
                            return source;
                        });
                        self.targetResources(resources);
                        self.reportDataLoading(false);
                    });
            };
            setUpReports();
        };

        this.updateSearchResults = function(termFilter, pagingFilter) {
            getResultData(termFilter, pagingFilter);
        };

        this.selectedTerm.subscribe(function(val) {
            self.termFilter(self.termOptions.find(x => val == x.id));
            self.updateSearchResults(self.termFilter());
        });

        this.query.subscribe(function(query) {
            self.updateSearchResults(self.termFilter(), query['paging-filter']);
        });

        this.initialize();
    }

    ko.components.register('add-things-step', {
        viewModel: viewModel,
        template: {
            require: 'text!templates/views/components/workflows/create-project-workflow/add-things-step.htm'
        }
    });

    return viewModel;
});
