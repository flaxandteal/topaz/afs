define([
    'underscore',
    'jquery',
    'arches',
    'views/components/workflows/stepUtils',
    'knockout',
    'knockout-mapping',
    'utils/resource',
    'models/graph',
    'viewmodels/card',
    'views/components/iiif-annotation',
    'text!templates/views/components/iiif-popup.htm',
], function(_, $, arches, StepUtils, ko, koMapping, ResourceUtils, GraphModel, CardViewModel, IIIFAnnotationViewmodel, iiifPopup) {
    function viewModel(params) {
        var self = this;
        _.extend(this, params);

        this.environmentResourceId = koMapping.toJS(params.environmentResourceId);
        this.samplingActivityResourceId = koMapping.toJS(params.samplingActivityResourceId);
        
        var digitalResourceServiceIdentifierContentNodeId = '56f8e9bd-ca7c-11e9-b578-a4d18cec433a';
        const partIdentifierAssignmentPhysicalPartOfObjectNodeId = 'b240c366-8594-11ea-97eb-acde48001122';
        const samplingUnitNodegroupId = 'b3e171a7-1d9d-11eb-a29f-024e0d439fdb';
        const sampleMotivationConceptId = "7060892c-4d91-4ab3-b3de-a95e19931a61";
        const samplingActivityGraphId = "03357848-1d9d-11eb-a29f-024e0d439fdb";
        const environmentPartAnnotationNodeId = "97c30c42-8594-11ea-97eb-acde48001122";
        this.analysisAreaResourceIds = [];
        this.manifestUrl = ko.observable(params.imageServiceInstanceData[digitalResourceServiceIdentifierContentNodeId]);

        this.samplingActivitySamplingUnitCard = ko.observable();
        this.samplingActivityStatementCard = ko.observable();
        this.physThingSearchResultsLookup = {};
        this.savingTile = ko.observable();
        this.savingMessage = ko.observable();

        this.selectedFeature = ko.observable();
        this.featureLayers = ko.observableArray();
        this.isFeatureBeingEdited = ko.observable(false);

        this.environmentPartIdentifierAssignmentCard = ko.observable();
        this.environmentPartIdentifierAssignmentTile = ko.observable();

        this.partIdentifierAssignmentLabelWidget = ko.observable();
        this.partIdentifierAssignmentPolygonIdentifierWidget = ko.observable();

        this.previouslySavedSampleDescriptionWidgetValue = ko.observable();
        this.sampleDescriptionWidgetValue = ko.observable();

        this.previouslySavedMotivationForSamplingWidgetValue = ko.observable();
        this.motivationForSamplingWidgetValue = ko.observable();

        this.activeTab = ko.observable();
        this.hasExternalCardData = ko.observable(false);

        this.sampleLocationInstances = ko.observableArray();
        
        this.selectedSampleLocationInstance = ko.observable();
        this.selectedSampleRelatedSamplingActivity = ko.observable();

        this.sampleLocationDisabled = ko.computed(() => {
            if(self.samplingActivityResourceId != self.selectedSampleRelatedSamplingActivity()){
                return true;
            } else {
                return false;
            }
        })

        this.switchCanvas = function(tile){
            const features = ko.unwrap(tile.data[environmentPartAnnotationNodeId].features)
            const canvasPath = features?.[0]?.properties.canvas()
            if (self.canvas() !== canvasPath) {
                var canvas = self.canvases().find(c => c.images[0].resource.service['@id'] === canvasPath);
                if (canvas) {
                    self.canvasClick(canvas);       
                }
            }
        };

        this.selectedSampleLocationInstance.subscribe(function(selectedSampleLocationInstance) {
            self.highlightAnnotation();

            if (selectedSampleLocationInstance) {
                self.tile = selectedSampleLocationInstance;
                params.tile = selectedSampleLocationInstance;
                self.environmentPartIdentifierAssignmentTile(selectedSampleLocationInstance);
                if (ko.unwrap(ko.unwrap(selectedSampleLocationInstance.data[environmentPartAnnotationNodeId])?.features)) {
                    self.switchCanvas(selectedSampleLocationInstance)
                }
            }
        });

        this.tileDirty = ko.computed(function() {
            if(self.sampleLocationDisabled()){
                return false;
            }
            if (
                self.sampleDescriptionWidgetValue() && self.sampleDescriptionWidgetValue() !== self.previouslySavedSampleDescriptionWidgetValue()
            ) {
                return true;
            }
            else if (
                self.motivationForSamplingWidgetValue() && self.motivationForSamplingWidgetValue() !== self.previouslySavedMotivationForSamplingWidgetValue()
            ) {
                return true;
            }
            else if (self.environmentPartIdentifierAssignmentTile()) {
                return self.environmentPartIdentifierAssignmentTile().dirty();
            }
        });

        this.selectedSampleLocationInstanceFeatures = ko.computed(function() {
            var partIdentifierAssignmentPolygonIdentifierNodeId = "97c30c42-8594-11ea-97eb-acde48001122";  // Part Identifier Assignment_Polygon Identifier (E42)

            if (self.selectedSampleLocationInstance()) {
                if (ko.unwrap(self.selectedSampleLocationInstance().data[partIdentifierAssignmentPolygonIdentifierNodeId])) {
                    var partIdentifierAssignmentPolygonIdentifierData = ko.unwrap(self.selectedSampleLocationInstance().data[partIdentifierAssignmentPolygonIdentifierNodeId]);
                    return ko.unwrap(partIdentifierAssignmentPolygonIdentifierData.features);
                }
            }
        });

        this.sampleLocationFilterTerm = ko.observable();
        this.filteredSampleLocationInstances = ko.computed(function() {
            const samplesOnly = self.sampleLocationInstances().filter(function(a){
                const analysisAreaResourceIds = self.analysisAreaResourceIds.map(item => item.resourceid);
                const partId = ko.unwrap(a.data[partIdentifierAssignmentPhysicalPartOfObjectNodeId]()[0].resourceId)
                return !analysisAreaResourceIds.includes(partId);
            });
            if (self.sampleLocationFilterTerm()) {
                return samplesOnly.filter(function(sampleLocationInstance) {
                    var partIdentifierAssignmentLabelNodeId = '3e541cc6-859b-11ea-97eb-acde48001122';
                    return sampleLocationInstance.data[partIdentifierAssignmentLabelNodeId]().includes(self.sampleLocationFilterTerm());
                });
            }
            else {
                return samplesOnly;
            }
        });

        this.hasExternalCardData.subscribe(function(hasExternalCardData) {
            if (hasExternalCardData) {
                self.handleExternalCardData();

                var environmentGeometriestAnnotationSubscription = self.annotationNodes.subscribe(function(annotationNodes) {
                    self.setEnvironmentGeometriesToVisible(annotationNodes);
                    environmentGeometriestAnnotationSubscription.dispose(); /* self-disposing subscription only runs once */
                });

                self.activeTab('dataset');

                self.manifest(self.manifestUrl());
                self.getManifestData();
            }
        });

        this.sampleName = ko.computed(function() {
            var partIdentifierAssignmentLabelNodeId = '3e541cc6-859b-11ea-97eb-acde48001122';
            if (self.selectedSampleLocationInstance()){
                const baseName = self.selectedSampleLocationInstance().data[partIdentifierAssignmentLabelNodeId]() || "";
                return `${baseName} [Sample of ${params.environmentName}]`;
            }
        })

        this.initialize = function() {
            params.form.save = self.saveWorkflowStep;

            $.getJSON(arches.urls.api_card + self.environmentResourceId).then(function(data) {
                self.loadExternalCardData(data);
            });

            self.fetchCardFromResourceId(self.samplingActivityResourceId, samplingUnitNodegroupId).then(function(samplingActivitySamplingUnitCard) {
                self.samplingActivitySamplingUnitCard(samplingActivitySamplingUnitCard);
            });

            // var samplingActivityStatementNodegroupId = '0335786d-1d9d-11eb-a29f-024e0d439fdb';  // Statement (E33)

            // self.fetchCardFromResourceId(self.samplingActivityResourceId, samplingActivityStatementNodegroupId).then(function(samplingActivityStatementCard) {
            //     self.samplingActivityStatementCard(samplingActivityStatementCard);
            // });
        };

        this.getSampleLocationTileFromFeatureId = function(featureId) {
            var partIdentifierAssignmentPolygonIdentifierNodeId = "97c30c42-8594-11ea-97eb-acde48001122";  // Part Identifier Assignment_Polygon Identifier (E42)

            return self.sampleLocationInstances().find(function(sampleLocationInstance) {
                var sampleLocationInstanceFeatures = sampleLocationInstance.data[partIdentifierAssignmentPolygonIdentifierNodeId].features();

                return sampleLocationInstanceFeatures.find(function(sampleLocationInstanceFeature) {
                    return ko.unwrap(sampleLocationInstanceFeature.id) === featureId;
                });
            });
        };

        this.getAnnotationProperty = function(tile, property){
            return tile.data[self.annotationNodeId].features[0].properties[property];
        };

        this.highlightAnnotation = function(){
            if (self.map()) {
                self.map().eachLayer(function(layer){
                    if (layer.eachLayer) {
                        layer.eachLayer(function(features){
                            if (features.eachLayer) {
                                features.eachLayer(function(feature) {
                                    var defaultColor = feature.feature.properties.color;
                                    
                                    if (self.selectedSampleLocationInstance() && self.selectedSampleLocationInstance().tileid === feature.feature.properties.tileId) {
                                        feature.setStyle({color: '#BCFE2B', fillColor: '#BCFE2B'});
                                    } else {
                                        feature.setStyle({color: defaultColor, fillColor: defaultColor});
                                    }
                                });
                            }
                        });
                    }
                })
            } 
        };

        this.removeFeatureFromCanvas = function(feature) {
            var annotationNodes = self.annotationNodes();
            
            var environmentAnnotationNodeName = "Sample Locations";
            var environmentAnnotationNode = annotationNodes.find(function(annotationNode) {
                return annotationNode.name === environmentAnnotationNodeName;
            });

            var filteredEnvironmentAnnotationNodeAnnotations = environmentAnnotationNode.annotations().filter(function(annotation) {
                return ko.unwrap(feature.id) !== annotation.id;
            });

            environmentAnnotationNode.annotations(filteredEnvironmentAnnotationNodeAnnotations);

            var environmentAnnotationNodeIndex = annotationNodes.findIndex(function(annotationNode) {
                return annotationNode.name === environmentAnnotationNodeName;
            });

            annotationNodes[environmentAnnotationNodeIndex] = environmentAnnotationNode;

            self.annotationNodes(annotationNodes);

            self.highlightAnnotation();
        }

        this.resetCanvasFeatures = function() {
            var annotationNodes = self.annotationNodes();
            
            if (self.selectedSampleLocationInstanceFeatures()) {
                var environmentAnnotationNodeName = "Sample Locations";
                var environmentAnnotationNode = annotationNodes.find(function(annotationNode) {
                    return annotationNode.name === environmentAnnotationNodeName;
                });
    
                var environmentAnnotationNodeAnnotationIds = environmentAnnotationNode.annotations().map(function(annotation) {
                    return ko.unwrap(annotation.id);
                });
                
                var unaddedSelectedSampleLocationInstanceFeatures = self.selectedSampleLocationInstanceFeatures().reduce(function(acc, feature) {
                    if (!environmentAnnotationNodeAnnotationIds.includes(ko.unwrap(feature.id)) &&
                        feature.properties.canvas === self.canvas) {
                        feature.properties.tileId = self.selectedSampleLocationInstance().tileid;
                        acc.push(ko.toJS(feature));
                    }
    
                    return acc;
                }, []);
    
                environmentAnnotationNode.annotations([...environmentAnnotationNode.annotations(), ...unaddedSelectedSampleLocationInstanceFeatures]);
    
                var environmentAnnotationNodeIndex = annotationNodes.findIndex(function(annotationNode) {
                    return annotationNode.name === environmentAnnotationNodeName;
                });
    
                annotationNodes[environmentAnnotationNodeIndex] = environmentAnnotationNode;
            }

            self.annotationNodes(annotationNodes);
        };

        this.updateSampleLocationInstances = function() {
            canvasids = self.canvases().map(canvas => canvas.images[0].resource['@id'])
            const tilesBelongingToManifest = self.card.tiles().filter(
                tile => canvasids.find(
                    canvas => canvas.startsWith(tile.data[environmentPartAnnotationNodeId].features()[0].properties.canvas())
                    )
                );

            tilesBelongingToManifest.forEach(tile => tile.samplingActivityResourceId = tile.samplingActivityResourceId ? tile.samplingActivityResourceId : ko.observable());
            self.sampleLocationInstances(tilesBelongingToManifest);
        };

        this.sampleLocationInstances.subscribe(async (instances) => {
            for(const instance of instances) {
                const instanceResourceId = ko.unwrap(ko.unwrap(instance.data[partIdentifierAssignmentPhysicalPartOfObjectNodeId])?.[0]?.resourceId);
                const currentResourceRelatedResources = await(await window.fetch(`${arches.urls.related_resources}${instanceResourceId}`)).json();
                const relatedSamplingActivity = currentResourceRelatedResources?.related_resources?.related_resources?.filter(x => x?.graph_id == samplingActivityGraphId);
                instance.samplingActivityResourceId(relatedSamplingActivity?.[0]?.resourceinstanceid);
            }
        });

        this.selectSampleLocationInstance = async function(sampleLocationInstance) {
            self.sampleDescriptionWidgetValue(null);
            self.previouslySavedSampleDescriptionWidgetValue(null);
            
            self.motivationForSamplingWidgetValue(null);
            self.previouslySavedMotivationForSamplingWidgetValue(null);

            var previouslySelectedSampleLocationInstance = self.selectedSampleLocationInstance();

            /* resets any changes not explicity saved to the tile */ 
            if (previouslySelectedSampleLocationInstance && previouslySelectedSampleLocationInstance.tileid !== sampleLocationInstance.tileid) {
                previouslySelectedSampleLocationInstance.reset();

                self.drawFeatures([]);
                self.resetCanvasFeatures();
            }

            if (self.environmentPartIdentifierAssignmentTile()) {
                self.environmentPartIdentifierAssignmentTile().reset();
            }

            self.selectedSampleLocationInstance(sampleLocationInstance);

            if (self.selectedSampleLocationInstance()) {

                var selectedSampleLocationParentEnvironmentData = ko.unwrap(self.selectedSampleLocationInstance().data[partIdentifierAssignmentPhysicalPartOfObjectNodeId]);
                
                var selectedSampleLocationParentEnvironmentResourceId;
                if (selectedSampleLocationParentEnvironmentData) {
                    selectedSampleLocationParentEnvironmentResourceId = ko.unwrap(selectedSampleLocationParentEnvironmentData[0].resourceId);
                } 

                let samplingActivitySamplingUnitCard = self.samplingActivitySamplingUnitCard();
                if(self.selectedSampleLocationInstance()?.samplingActivityResourceId) {
                    self.selectedSampleRelatedSamplingActivity(self.selectedSampleLocationInstance().samplingActivityResourceId());
                } else if (selectedSampleLocationParentEnvironmentResourceId){
                    const selectedResourceRelatedResources = await(await window.fetch(`${arches.urls.related_resources}${selectedSampleLocationParentEnvironmentResourceId}`)).json();
                    const relatedSamplingActivity = selectedResourceRelatedResources?.related_resources?.related_resources?.filter(x => x?.graph_id == samplingActivityGraphId);
                    self.selectedSampleRelatedSamplingActivity(relatedSamplingActivity?.[0].resourceinstanceid);
                } else {
                    self.selectedSampleRelatedSamplingActivity(self.samplingActivityResourceId);
                }

                if(self.selectedSampleRelatedSamplingActivity() && self.selectedSampleRelatedSamplingActivity() != self.samplingActivityResourceId) {
                    samplingActivitySamplingUnitCard = await self.fetchCardFromResourceId(self.selectedSampleRelatedSamplingActivity(), samplingUnitNodegroupId);
                }

                if(!samplingActivitySamplingUnitCard) { return; }

                var samplingAreaNodeId = 'b3e171ac-1d9d-11eb-a29f-024e0d439fdb';  // Sampling Area (E22)
                var samplingActivitySamplingUnitTile = samplingActivitySamplingUnitCard.tiles().find(function(tile) {
                    var data = ko.unwrap(tile.data[samplingAreaNodeId]);

                    if (data) {
                        return ko.unwrap(data[0].resourceId) === selectedSampleLocationParentEnvironmentResourceId;
                    }
                });

                if (samplingActivitySamplingUnitTile) {
                    var samplingAreaSampleCreatedNodeId = 'b3e171ab-1d9d-11eb-a29f-024e0d439fdb';  // Sample Created (E22)
                    var samplingAreaSampleCreatedParentEnvironmentResourceId = ko.unwrap(samplingActivitySamplingUnitTile.data[samplingAreaSampleCreatedNodeId])[0].resourceId();
                    
                    var environmentStatementNodegroupId = '1952bb0a-b498-11e9-a679-a4d18cec433a';  // Statement (E33)
    
                    self.fetchCardFromResourceId(samplingAreaSampleCreatedParentEnvironmentResourceId, environmentStatementNodegroupId).then(function(samplingAreaSampleCreatedParentEnvironmentStatementCard) {
                        var environmentStatementTypeNodeId = '1952e470-b498-11e9-b261-a4d18cec433a'; // Statement_type (E55)
                        var environmentStatementContentNodeId = '1953016e-b498-11e9-9445-a4d18cec433a';  // Statement_content (xsd:string)

                        var sampleDescriptionConceptId = "9886efe9-c323-49d5-8d32-5c2a214e5630";

                        var sampleDescriptionTile = samplingAreaSampleCreatedParentEnvironmentStatementCard.tiles().find(function(tile) {
                            return ko.unwrap(tile.data[environmentStatementTypeNodeId]).includes(sampleDescriptionConceptId);
                        });
        
                        var samplingMotivationTile = samplingAreaSampleCreatedParentEnvironmentStatementCard.tiles().find(function(tile) {
                            return ko.unwrap(tile.data[environmentStatementTypeNodeId]).includes(sampleMotivationConceptId);
                        });

                        if (sampleDescriptionTile) {
                            var sampleDescriptionContent = ko.unwrap(sampleDescriptionTile.data[environmentStatementContentNodeId]);

                            self.sampleDescriptionWidgetValue(sampleDescriptionContent);
                            self.previouslySavedSampleDescriptionWidgetValue(sampleDescriptionContent);
                        }

                        if (samplingMotivationTile) {
                            var sampleMotivationContent = ko.unwrap(samplingMotivationTile.data[environmentStatementContentNodeId]);
            
                            self.motivationForSamplingWidgetValue(sampleMotivationContent);
                            self.previouslySavedMotivationForSamplingWidgetValue(sampleMotivationContent);
                        }
                    });
                }
            }
        };

        this.resetDescriptions = function(){
            const previouslySavedSampleDescriptionWidgetValue = ko.unwrap(self.previouslySavedSampleDescriptionWidgetValue);
            const previouslySavedMotivationForSamplingWidgetValue = ko.unwrap(self.previouslySavedMotivationForSamplingWidgetValue);
            
            self.sampleDescriptionWidgetValue(previouslySavedSampleDescriptionWidgetValue);
            self.motivationForSamplingWidgetValue(previouslySavedMotivationForSamplingWidgetValue);

        }

        this.resetSampleLocationTile = function() {
            self.tile.reset();
            self.resetCanvasFeatures();
            self.resetDescriptions();
            self.drawFeatures([]);
            self.highlightAnnotation();
            self.selectedFeature(null);
        };

        this.setEnvironmentGeometriesToVisible = function(annotationNodes) {
            var environmentAnnotationNodeName = "Sample Locations";
            var environmentAnnotationNode = annotationNodes.find(function(annotationNode) {
                return annotationNode.name === environmentAnnotationNodeName;
            });

            environmentAnnotationNode.active(true); 
            self.updateSampleLocationInstances();
        };

        this.saveSampleLocationTile = function() {
            // don't save if tile isn't dirty.
            if(!self.tileDirty()){ return; }

            var partIdentifierAssignmentLabelNodeId = '3e541cc6-859b-11ea-97eb-acde48001122';
            var partIdentifierAssignmentPolygonIdentifierNodeId = "97c30c42-8594-11ea-97eb-acde48001122"
            const featureCollection = ko.unwrap(self.selectedSampleLocationInstance().data[partIdentifierAssignmentPolygonIdentifierNodeId])
            if (!ko.unwrap(featureCollection?.features)?.length ||
                !self.selectedSampleLocationInstance().data[partIdentifierAssignmentLabelNodeId]()) { //Sample Name Node
                    params.pageVm.alert(new params.form.AlertViewModel(
                        "ep-alert-red",
                        "Missing Values",
                        "Sample Location and Sample Name are Required",
                    ));
                    return;    
                }

            var saveEnvironmentNameTile = function(environmentNameTile, type) {
                return new Promise(function(resolve, _reject) {
                    var selectedSampleLocationInstanceLabel = ko.unwrap(self.sampleName);
                    var environmentNameContentNodeId = 'b9c1d8a6-b497-11e9-876b-a4d18cec433a'; // Name_content (xsd:string)

                    if (type === "region") {
                        const baseName = self.selectedSampleLocationInstance().data[partIdentifierAssignmentLabelNodeId]() || "";
                        selectedSampleLocationInstanceLabel = `${baseName} [Sample Area of ${params.environmentName}]`;
                    }
                    
                    environmentNameTile.data[environmentNameContentNodeId] = selectedSampleLocationInstanceLabel;
                    environmentNameTile.transactionId = params.form.workflowId;

                    environmentNameTile.save().then(function(environmentNameData) {
                        resolve(environmentNameData);
                    });
                });
            };

            const saveEnvironmentClassificationTile = function(environmentClassificationTile, type) {
                const sampleLocationTypeConceptId = '7375a6fb-0bfb-4bcf-81a3-6180cdd26123';
                const sampleTypeConceptId = '77d8cf19-ce9c-4e0a-bde1-9148d870e11c';
                return new Promise(function(resolve, _reject) {
                    const environmentClassificationNodeId = '8ddfe3ab-b31d-11e9-aff0-a4d18cec433a'; // type (E55)
                    if (type === "region") {
                        environmentClassificationTile.data[environmentClassificationNodeId] = [sampleLocationTypeConceptId];
                    }
                    else { // type === "sample"
                        environmentClassificationTile.data[environmentClassificationNodeId] = [sampleTypeConceptId];
                    }
                    environmentClassificationTile.transactionId = params.form.workflowId;

                    environmentClassificationTile.save().then(function(environmentClassificationData) {
                        resolve(environmentClassificationData);
                    });
                });
            };

            var saveEnvironmentPartOfTile = function(environmentPartOfTile) {
                var environmentPartOfNodeId = 'f8d5fe4c-b31d-11e9-9625-a4d18cec433a'; // part of (E22)

                return new Promise(function(resolve, _reject) {
                    environmentPartOfTile.data[environmentPartOfNodeId] = [{
                        "resourceId": self.environmentResourceId,
                        "ontologyProperty": "",
                        "inverseOntologyProperty": ""
                    }];
                    environmentPartOfTile.transactionId = params.form.workflowId;

                    environmentPartOfTile.save().then(function(environmentPartOfData) {
                        resolve(environmentPartOfData);
                    });
                });
            };

            var saveSelectedSampleLocationInstance = function(environmentPartOfData) {
                return new Promise(function(resolve, _reject) {
                    /* assigns Physical Thing to be the Part Identifier on the parent selected Physical Thing  */ 
                    var environmentPartOfNodeId = 'f8d5fe4c-b31d-11e9-9625-a4d18cec433a'; // part of (E22)
                    var environmentPartOfResourceXResourceId = environmentPartOfData.data[environmentPartOfNodeId][0]['resourceXresourceId'];
                    
                    var selectedSampleLocationInstance = self.selectedSampleLocationInstance();
    
                    selectedSampleLocationInstance.data[partIdentifierAssignmentPhysicalPartOfObjectNodeId]([{
                        "resourceId": environmentPartOfData.resourceinstance_id,
                        "resourceXresourceId": environmentPartOfResourceXResourceId,
                        "ontologyProperty": "",
                        "inverseOntologyProperty": ""
                    }]);
                    selectedSampleLocationInstance.transactionId = params.form.workflowId
    
                    selectedSampleLocationInstance.save().then(function(data) {
                        resolve(data);
                    });
                });
            };

            var updateAnnotations = function() {
                return new Promise(function(resolve, _reject) {
                    /* updates selected annotations */ 
                    var environmentAnnotationNodeName = "Sample Locations";
                    var environmentAnnotationNode = self.annotationNodes().find(function(annotationNode) {
                        return annotationNode.name === environmentAnnotationNodeName;
                    });
    
                    var environmentAnnotations = environmentAnnotationNode.annotations();
    
                    self.drawFeatures().forEach(function(drawFeature) {
                        var annotationFeature = environmentAnnotations.find(function(annotation) {
                            return annotation.id === drawFeature;
                        });
    
                        drawFeature.properties.nodegroupId = self.tile.nodegroup_id;
                        drawFeature.properties.resourceId = self.tile.resourceinstance_id;
                        drawFeature.properties.tileId = self.tile.tileid;
    
                        if (!annotationFeature) {
                            environmentAnnotations.push(drawFeature);
                        }
                    });
    
                    environmentAnnotationNode.annotations(environmentAnnotations);

                    resolve(environmentAnnotationNode)
                });
            };

            var getWorkingTile = function(card) {
                /* 
                    If an auto-generated resource has a tile with data, this will return it.
                    Otherwise it returns a new tile for the card.
                */ 

                var tile = null;
                
                /* Since this is an autogenerated resource, we can assume only one associated tile. */ 
                if (card.tiles() && card.tiles().length) {
                    tile = card.tiles()[0];
                }
                else {
                    tile = card.getNewTile();
                }

                return tile;
            };

            var getWorkingSamplingActivityUnitTile = function(samplingActivitySamplingUnitCard, regionEnvironmentNameData) {
                var samplingAreaNodeId = 'b3e171ac-1d9d-11eb-a29f-024e0d439fdb';  // Sampling Area (E22)

                var samplingActivitySamplingUnitTile;
                if (samplingActivitySamplingUnitCard.tiles() && samplingActivitySamplingUnitCard.tiles().length) {
                    var previouslySavedTile = samplingActivitySamplingUnitCard.tiles().find(function(tile) {
                        var data = ko.unwrap(tile.data[samplingAreaNodeId]);

                        if (data) {
                            return ko.unwrap(data[0].resourceId) === regionEnvironmentNameData.resourceinstance_id;
                        }
                    });

                    if (previouslySavedTile) {
                        samplingActivitySamplingUnitTile = previouslySavedTile;
                    }
                    else {
                        samplingActivitySamplingUnitTile = samplingActivitySamplingUnitCard.getNewTile();
                    }
                }
                else {
                    samplingActivitySamplingUnitTile = samplingActivitySamplingUnitCard.getNewTile();
                }

                return samplingActivitySamplingUnitTile;
            };

            var saveSamplingActivitySamplingUnitTile = function(samplingActivitySamplingUnitTile, regionEnvironmentNameData, sampleEnvironmentNameData) {
                return new Promise(function(resolve, _reject) {
                    var samplingAreaNodeId = 'b3e171ac-1d9d-11eb-a29f-024e0d439fdb';  // Sampling Area (E22)
                    
                    samplingActivitySamplingUnitTile.data[samplingAreaNodeId] = [{
                        "resourceId": regionEnvironmentNameData.resourceinstance_id,
                        "ontologyProperty": "",
                        "inverseOntologyProperty": ""
                    }];

                    var samplingAreaOverallObjectSampledNodeId = 'b3e171aa-1d9d-11eb-a29f-024e0d439fdb';  //  Overall Object Sampled (E22)
                    samplingActivitySamplingUnitTile.data[samplingAreaOverallObjectSampledNodeId] = [{
                        "resourceId": self.environmentResourceId,
                        "ontologyProperty": "",
                        "inverseOntologyProperty": ""
                    }];

                    var samplingAreaSampleCreatedNodeId = 'b3e171ab-1d9d-11eb-a29f-024e0d439fdb';  // Sample Created (E22)
                    samplingActivitySamplingUnitTile.data[samplingAreaSampleCreatedNodeId] = [{
                        "resourceId": sampleEnvironmentNameData.resourceinstance_id,
                        "ontologyProperty": "",
                        "inverseOntologyProperty": ""
                    }];

                    var partIdentifierAssignmentPolygonIdentifierNodeId = "97c30c42-8594-11ea-97eb-acde48001122";  // Part Identifier Assignment_Polygon Identifier (E42)
                    var samplingAreaVisualizationNodeId = 'b3e171ae-1d9d-11eb-a29f-024e0d439fdb';  // Sampling Area Visualization (E42)

                    samplingActivitySamplingUnitTile.data[samplingAreaVisualizationNodeId] = ko.toJS(
                        self.environmentPartIdentifierAssignmentTile().data[partIdentifierAssignmentPolygonIdentifierNodeId]
                    );
                    samplingActivitySamplingUnitTile.transactionId = params.form.workflowId;

                    samplingActivitySamplingUnitTile.save().then(function(data) {
                        resolve(data);
                    });
                });
            };

            var getRegionEnvironmentNameCard = function() {
                return new Promise(function(resolve, _reject) {
                    var environmentNameNodegroupId = 'b9c1ced7-b497-11e9-a4da-a4d18cec433a';  // Name (E33)
                    var partIdentifierAssignmentPhysicalPartOfObjectData = ko.unwrap(self.tile.data[partIdentifierAssignmentPhysicalPartOfObjectNodeId]);
        
                    if (partIdentifierAssignmentPhysicalPartOfObjectData) { /* if editing Physical Thing */
                        var partIdentifierAssignmentPhysicalPartOfObjectResourceId = partIdentifierAssignmentPhysicalPartOfObjectData[0]['resourceId']();
        
                        self.fetchCardFromResourceId(partIdentifierAssignmentPhysicalPartOfObjectResourceId, environmentNameNodegroupId).then(function(environmentNameCard) {
                            resolve(environmentNameCard);
                        });
                    }
                    else {
                        var environmentGraphId = '9519cb4f-b25b-11e9-8c7b-a4d18cec433a';
        
                        self.fetchCardFromGraphId(environmentGraphId, environmentNameNodegroupId).then(function(environmentNameCard) { 
                            resolve(environmentNameCard);
                        });
                    }

                });
            };

            var getSampleEnvironmentNameCard = function(samplingActivitySamplingUnitTile) {
                return new Promise(function(resolve, _reject) {
                    var samplingUnitSampleCreatedNodeId = 'b3e171ab-1d9d-11eb-a29f-024e0d439fdb';  // Sample Created (E22)
                    var samplingUnitSampleCreatedData = ko.unwrap(samplingActivitySamplingUnitTile.data[samplingUnitSampleCreatedNodeId]);
    
                    var environmentNameNodegroupId = 'b9c1ced7-b497-11e9-a4da-a4d18cec433a';  // Name (E33)

                    if (samplingUnitSampleCreatedData) {
                        /* name card of physical thing representing sample */ 
                        self.fetchCardFromResourceId(samplingUnitSampleCreatedData[0].resourceId(), environmentNameNodegroupId).then(function(samplingUnitSampleCreatedCard) {
                            resolve(samplingUnitSampleCreatedCard);
                        });
                    }
                    else {
                        var environmentGraphId = '9519cb4f-b25b-11e9-8c7b-a4d18cec433a';
    
                        self.fetchCardFromGraphId(environmentGraphId, environmentNameNodegroupId).then(function(samplingUnitSampleCreatedCard) { 
                            resolve(samplingUnitSampleCreatedCard);
                        });
                    }
                });              
            };

            var getWorkingEnvironmentSamplingDescriptionTile = function(environmentStatementCard) {
                var sampleDescriptionConceptId = "9886efe9-c323-49d5-8d32-5c2a214e5630"
                var environmentStatementTypeNodeId = "1952e470-b498-11e9-b261-a4d18cec433a"; // Statement_type (E55)

                if (environmentStatementCard.tiles() && environmentStatementCard.tiles().length) {

                    var previouslySavedTile = environmentStatementCard.tiles().find(function(tile) {
                        return ko.unwrap(tile.data[environmentStatementTypeNodeId]).includes(sampleDescriptionConceptId);
                    });

                    if (previouslySavedTile) {
                        return previouslySavedTile;
                    }
                    else {
                        return environmentStatementCard.getNewTile();
                    }
                }
                else {
                    return environmentStatementCard.getNewTile();
                }
            };

            var getWorkingEnvironmentSamplingMotivationTile = function(environmentStatementCard) {
                var environmentStatementTypeNodeId = "1952e470-b498-11e9-b261-a4d18cec433a"; // Statement_type (E55)

                if (environmentStatementCard.tiles() && environmentStatementCard.tiles().length) {

                    var previouslySavedTile = environmentStatementCard.tiles().find(function(tile) {
                        return ko.unwrap(tile.data[environmentStatementTypeNodeId]).includes(sampleMotivationConceptId);
                    });

                    if (previouslySavedTile) {
                        return previouslySavedTile;
                    }
                    else {
                        return environmentStatementCard.getNewTile();
                    }
                }
                else {
                    return environmentStatementCard.getNewTile();
                }
            };


            var saveEnvironmentStatementTile = function(environmentStatementTile, type) {
                var sampleDescriptionConceptId = "9886efe9-c323-49d5-8d32-5c2a214e5630";

                return new Promise(function(resolve, _reject) {
                    if (self.sampleDescriptionWidgetValue()) {
                        var environmentStatementContentNodeId = '1953016e-b498-11e9-9445-a4d18cec433a';  // Statement_content (xsd:string)
                        var environmentStatementTypeNodeId = '1952e470-b498-11e9-b261-a4d18cec433a'; // Statement_type (E55)
    
                        var environmentStatementTypeData = ko.unwrap(environmentStatementTile.data[environmentStatementTypeNodeId]);

                        if (type === "description") {
                            environmentStatementTile.data[environmentStatementContentNodeId] = self.sampleDescriptionWidgetValue();
                            if (!environmentStatementTypeData.includes(sampleDescriptionConceptId)) {
                                environmentStatementTypeData = [sampleDescriptionConceptId];
                            }
                        } else if (type === "motivation") {
                            environmentStatementTile.data[environmentStatementContentNodeId] = self.motivationForSamplingWidgetValue();
                            if (!environmentStatementTypeData.includes(sampleMotivationConceptId)) {
                                environmentStatementTypeData = [sampleMotivationConceptId];
                            }
                        }
    
                        environmentStatementTile.data[environmentStatementTypeNodeId] = environmentStatementTypeData;
                        environmentStatementTile.transactionId = params.form.workflowId;

                        environmentStatementTile.save().then(function(data) {
                            resolve(data);
                        });
                    }
                    else {
                        resolve(null);
                    }
                });
            };

            const saveEnvironmentRemovedFromTile = function(sampleEnvironmentRemovedFromTile, removedFromResourceInstanceIds) {
                return new Promise(function(resolve, _reject) {
                    const environmentRemovedFromNodeId = '38814345-d2bd-11e9-b9d6-a4d18cec433a' // Removal from Object_Removed From (E22)
                    const tileDataObj = removedFromResourceInstanceIds.map(resourceId => 
                        ({
                            "resourceId": resourceId,
                            "resourceXresourceId": "",
                            "ontologyProperty": "",
                            "inverseOntologyProperty": ""
                        })
                    );
                    sampleEnvironmentRemovedFromTile.data[environmentRemovedFromNodeId](tileDataObj)
                    sampleEnvironmentRemovedFromTile.transactionId = params.form.workflowId;

                    sampleEnvironmentRemovedFromTile.save().then(function(data) {
                        resolve(data);
                    });
                });
            }

            self.savingTile(true);
            getRegionEnvironmentNameCard().then(function(regionEnvironmentNameCard) {
                var regionEnvironmentNameTile = getWorkingTile(regionEnvironmentNameCard);

                self.savingMessage(`Saving Sample Area Name ...`);
                saveEnvironmentNameTile(regionEnvironmentNameTile, "region").then(function(regionEnvironmentNameData) {
                    const environmentClassificationNodeId = '8ddfe3ab-b31d-11e9-aff0-a4d18cec433a'; // type (E55)

                    StepUtils.saveThingToProject(regionEnvironmentNameData.resourceinstance_id, params.projectSet, params.form.workflowId, self.physThingSearchResultsLookup).then(function() {

                    self.fetchCardFromResourceId(regionEnvironmentNameData.resourceinstance_id, environmentClassificationNodeId).then(function(regionEnvironmentClassificationCard) {
                        var environmentClassificationTile = getWorkingTile(regionEnvironmentClassificationCard);

                        self.savingMessage(`Saving Sample Area Classification ...`);
                        saveEnvironmentClassificationTile(environmentClassificationTile, "region").then(function(regionEnvironmentClassificationData) {
                            var environmentPartOfNodeId = 'f8d5fe4c-b31d-11e9-9625-a4d18cec433a'; // part of (E22)

                            self.fetchCardFromResourceId(regionEnvironmentClassificationData.resourceinstance_id, environmentPartOfNodeId).then(function(regionEnvironmentPartOfCard) {
                                var environmentPartOfTile = getWorkingTile(regionEnvironmentPartOfCard);

                                self.savingMessage(`Saving Relationship between Sample Area and Parent (${params.environmentName}) ...`);
                                saveEnvironmentPartOfTile(environmentPartOfTile).then(function(regionEnvironmentPartOfData) {
                                    self.fetchCardFromResourceId(self.samplingActivityResourceId, samplingUnitNodegroupId).then(function(samplingActivitySamplingUnitCard) {
                                        var samplingActivitySamplingUnitTile = getWorkingSamplingActivityUnitTile(samplingActivitySamplingUnitCard, regionEnvironmentNameData);
                    
                                        getSampleEnvironmentNameCard(samplingActivitySamplingUnitTile).then(function(sampleEnvironmentNameCard) {
                                            var sampleEnvironmentNameTile = getWorkingTile(sampleEnvironmentNameCard);

                                            self.savingMessage(`Saving Sample Name ...`);
                                            saveEnvironmentNameTile(sampleEnvironmentNameTile, "sample").then(function(sampleEnvironmentNameData) {
                                                const environmentRemovedFromNodegroupId = 'b11f217a-d2bc-11e9-8dfa-a4d18cec433a' // Removal from Object (E80)

                                                self.fetchCardFromResourceId(sampleEnvironmentNameData.resourceinstance_id, environmentPartOfNodeId).then(function(sampleEnvironmentPartOfCard){
                                                    const sampleEnvironmentPartOfTile = getWorkingTile(sampleEnvironmentPartOfCard);
                                                    self.savingMessage(`Saving Relationship between Sample and Parent (${params.environmentName}) ...`);
                                                    saveEnvironmentPartOfTile(sampleEnvironmentPartOfTile);
                                                });

                                                self.fetchCardFromResourceId(sampleEnvironmentNameData.resourceinstance_id, environmentRemovedFromNodegroupId).then(function(sampleEnvironmentRemovedFromCard){
                                                    const sampleEnvironmentRemovedFromTile = getWorkingTile(sampleEnvironmentRemovedFromCard);
                                                    const removedFromEnvironmentResourceIds = [self.environmentResourceId, regionEnvironmentNameData.resourceinstance_id];
                                                    self.savingMessage(`Saving Relationship between Sample and Sample Area ...`);
                                                    saveEnvironmentRemovedFromTile(sampleEnvironmentRemovedFromTile, removedFromEnvironmentResourceIds);
                                                });

                                                self.savingMessage(`Saving Sample to the Project ...`);
                                                StepUtils.saveThingToProject(sampleEnvironmentNameData.resourceinstance_id, params.projectSet, params.form.workflowId, self.physThingSearchResultsLookup).then(function() {

                                                const environmentClassificationNodeId = '8ddfe3ab-b31d-11e9-aff0-a4d18cec433a'; // type (E55)

                                                self.fetchCardFromResourceId(sampleEnvironmentNameData.resourceinstance_id, environmentClassificationNodeId).then(function(sampleEnvironmentClassificationCard) {
                                                    var environmentClassificationTile = getWorkingTile(sampleEnvironmentClassificationCard);
                            
                                                    self.savingMessage(`Saving Sample Classification ...`);
                                                    saveEnvironmentClassificationTile(environmentClassificationTile, "sample").then(function(sampleEnvironmentClassificationData) {
                                                        var environmentStatementNodegroupId = '1952bb0a-b498-11e9-a679-a4d18cec433a';  // Statement (E33)
                                                        
                                                        self.fetchCardFromResourceId(sampleEnvironmentClassificationData.resourceinstance_id, environmentStatementNodegroupId).then(function(environmentStatementCard) {
                                                            var environmentSampleDescriptionStatementTile = getWorkingEnvironmentSamplingDescriptionTile(environmentStatementCard);

                                                            self.savingMessage(`Saving Sample Description ...`);
                                                            saveEnvironmentStatementTile(environmentSampleDescriptionStatementTile, "description").then(function(_environmentStatmentSampleDescriptionData) {
                                                                var environmentStatementNodegroupId = '1952bb0a-b498-11e9-a679-a4d18cec433a';  // Statement (E33)

                                                                self.fetchCardFromResourceId(sampleEnvironmentNameData.resourceinstance_id, environmentStatementNodegroupId).then(function(environmentStatementCard) {
                                                                    var environmentSamplingMotivationTile = getWorkingEnvironmentSamplingMotivationTile(environmentStatementCard);

                                                                    self.savingMessage(`Saving Sample Motivation ...`);
                                                                    saveEnvironmentStatementTile(environmentSamplingMotivationTile, "motivation").then(function(_samplingActivitySamplingMotivationData) {
                                                                        self.savingMessage(`Saving Relationship between Sample and Sampling Activity ...`);
                                                                        saveSamplingActivitySamplingUnitTile(samplingActivitySamplingUnitTile, regionEnvironmentNameData, sampleEnvironmentNameData).then(function(_samplingActivitySamplingUnitData) {
                                                                            self.savingMessage(`Saving Relationship between Sample Area and Parent (${params.environmentName}) ...`);
                                                                            saveSelectedSampleLocationInstance(regionEnvironmentPartOfData).then(function(_selectedSampleLocationInstanceData) {
                                                                                // fetches again for updated data
                                                                                // self.fetchCardFromResourceId(self.samplingActivityResourceId, samplingUnitNodegroupId).then(function(updatedSamplingActivitySamplingUnitCard) {
                                                                                    updateAnnotations().then(function(_environmentAnnotationNode) {
                                                                                        self.samplingActivitySamplingUnitCard(samplingActivitySamplingUnitCard);
                                                                                        
                                                                                        self.updateSampleLocationInstances();
                                                                                        self.selectSampleLocationInstance(self.selectedSampleLocationInstance());
                            
                                                                                        self.savingTile(false);
                                                                                        self.savingMessage('');
                                                                                        params.dirty(true);
                                                                                        params.form.complete(true);

                                                                                        params.pageVm.alert("");
                                                                                        self.drawFeatures([]);
                                                                                    });
                                                                                // });
                                                                            });
                                                                        });
                                                                    });
                                                                });
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
};

        this.loadNewSampleLocationTile = function() {
            var newTile = self.card.getNewTile(true);  /* true flag forces new tile generation */
            self.selectSampleLocationInstance(newTile);
        };

        this.saveWorkflowStep = function() {
            params.form.complete(false);
            params.form.saving(true);
            let mappedInstances = self.sampleLocationInstances().map((instance) => { return { "data": instance.data }});
            params.form.savedData(mappedInstances);
            params.form.complete(true);
            params.form.saving(false);
        };

        this.loadExternalCardData = function(data) {
            var partIdentifierAssignmentNodeGroupId = 'fec59582-8593-11ea-97eb-acde48001122';  // Part Identifier Assignment (E13) 

            var partIdentifierAssignmentCardData = data.cards.find(function(card) {
                return card.nodegroup_id === partIdentifierAssignmentNodeGroupId;
            });

            var handlers = {
                'after-update': [],
                'tile-reset': []
            };

            var graphModel = new GraphModel({
                data: {
                    nodes: data.nodes,
                    nodegroups: data.nodegroups,
                    edges: []
                },
                datatypes: data.datatypes
            });

            var partIdentifierAssignmentCard = new CardViewModel({
                card: partIdentifierAssignmentCardData,
                graphModel: graphModel,
                tile: null,
                resourceId: ko.observable(self.environmentResourceId),
                displayname: ko.observable(data.displayname),
                handlers: handlers,
                cards: data.cards,
                tiles: data.tiles,
                cardwidgets: data.cardwidgets,
                userisreviewer: data.userisreviewer,
            });

            var card = partIdentifierAssignmentCard;
            var tile = partIdentifierAssignmentCard.getNewTile();

            self.card = card;
            self.tile = tile;

            params.card = self.card;
            params.tile = self.tile;
            
            var partIdentifierAssignmentPolygonIdentifierNodeId = "97c30c42-8594-11ea-97eb-acde48001122";  // Part Identifier Assignment_Polygon Identifier (E42)
            params.widgets = self.card.widgets().filter(function(widget) {
                return widget.node_id() === partIdentifierAssignmentPolygonIdentifierNodeId;
            });

            self.environmentPartIdentifierAssignmentCard(card);
            self.environmentPartIdentifierAssignmentTile(tile);

            const classificationNodeId = '8ddfe3ab-b31d-11e9-aff0-a4d18cec433a';
            const analysisAreaTypeConceptId = '31d97bdd-f10f-4a26-958c-69cb5ab69af1';
            const related = card.tiles().map((tile) => {
                return {
                    'resourceid': ko.unwrap(tile.data[partIdentifierAssignmentPhysicalPartOfObjectNodeId])[0].resourceId(),
                    'tileid': tile.tileid
                }
            });

            Promise.all(related.map(resource => ResourceUtils.lookupResourceInstanceData(resource.resourceid))).then((values) => {
                values.forEach((value) => {
                    self.physThingSearchResultsLookup[value._id] = value;
                    const nodevals = ResourceUtils.getNodeValues({
                        nodeId: classificationNodeId,
                        where: {
                            nodeId: classificationNodeId,
                            contains: analysisAreaTypeConceptId
                        }
                    }, value._source.tiles);
                    if (nodevals.includes(analysisAreaTypeConceptId)) {
                        self.analysisAreaResourceIds.push(related.find(tile => value._id === tile.resourceid));
                    }
                });
                card.tiles().forEach(tile => tile.samplingActivityResourceId = tile.samplingActivityResourceId ? tile.samplingActivityResourceId : ko.observable());
                self.sampleLocationInstances(card.tiles());
                self.analysisAreaTileIds = self.analysisAreaResourceIds.map(item => item.tileid);
            })
            /* 
                subscription to features lives here because we _only_ want it to run once, on blank starting tile, when a user places a feature on the map
            */
            var tileFeatureGeometrySubscription = tile.data[partIdentifierAssignmentPolygonIdentifierNodeId].subscribe(function(data) {
                if (data) {
                    self.selectSampleLocationInstance(tile);
                    tileFeatureGeometrySubscription.dispose();
                }
            });

            self.hasExternalCardData(true);
        };

        this.handleExternalCardData = function() {
            var partIdentifierAssignmentLabelNodeId = '3e541cc6-859b-11ea-97eb-acde48001122';
            self.partIdentifierAssignmentLabelWidget(self.card.widgets().find(function(widget) {
                return ko.unwrap(widget.node_id) === partIdentifierAssignmentLabelNodeId;
            }));

            var partIdentifierAssignmentPolygonIdentifierNodeId = '97c30c42-8594-11ea-97eb-acde48001122';
            self.partIdentifierAssignmentPolygonIdentifierWidget(self.card.widgets().find(function(widget) {
                return ko.unwrap(widget.node_id) === partIdentifierAssignmentPolygonIdentifierNodeId;
            }));                
            
            IIIFAnnotationViewmodel.apply(self, [{
                ...params,
                hideEditorTab: ko.observable(true),
                onEachFeature: function(feature, layer) {
                    var featureLayer = self.featureLayers().find(function(featureLayer) {
                        return featureLayer.feature.id === layer.feature.id;
                    });

                    if (!featureLayer) {
                        self.featureLayers.push(layer)
                    }

                    if (self.analysisAreaTileIds.includes(feature.properties.tileId)){
                        const analysisArea = self.analysisAreaResourceIds.find(analysisArea => analysisArea.tileid === feature.properties.tileId);
                        var popup = L.popup({
                            closeButton: false,
                            maxWidth: 349
                        })
                            .setContent(iiifPopup)
                            .on('add', function() {
                                var popupData = {
                                    'closePopup': function() {
                                        popup.remove();
                                    },
                                    'name': ko.observable(''),
                                    'description': ko.observable(''),
                                    'graphName': feature.properties.graphName,
                                    'resourceinstanceid': analysisArea.resourceid,
                                    'reportURL': arches.urls.resource_report
                                };
                                window.fetch(arches.urls.resource_descriptors + popupData.resourceinstanceid)
                                    .then(function(response) {
                                        return response.json();
                                    })
                                    .then(function(descriptors) {
                                        popupData.name(descriptors.displayname);
                                        const description = `<strong>Analysis Area</strong>
                                            <br>Analysis Areas may not be modified in the sample taking workflow
                                            <br>${descriptors['map_popup'] !== "Undefined" ? descriptors['map_popup'] : ''}`
                                        popupData.description(description);
                                    });
                                var popupElement = popup.getElement()
                                    .querySelector('.mapboxgl-popup-content');
                                ko.applyBindingsToDescendants(popupData, popupElement);
                            });
                        layer.bindPopup(popup);
                    }

                    layer.on({

                        click: function() {              
                            const sampleLocationInstance = self.getSampleLocationTileFromFeatureId(feature.id);
                            
                            if (sampleLocationInstance && !self.analysisAreaTileIds.includes(sampleLocationInstance.tileid)) {
                                self.drawFeatures([]);
                                self.featureClick = true;

                                if (!self.selectedSampleLocationInstance() || self.selectedSampleLocationInstance().tileid !== sampleLocationInstance.tileid ) {
                                    self.selectSampleLocationInstance(sampleLocationInstance);
                                }
                                else {
                                    self.tile.reset();
                                    self.resetCanvasFeatures();
    
                                    const selectedFeature = ko.toJS(self.selectedSampleLocationInstanceFeatures().find(function(selectedSampleLocationInstanceFeature) {
                                        return ko.unwrap(selectedSampleLocationInstanceFeature.id) === feature.id;
                                    }));
    
                                    self.selectedFeature(selectedFeature);
                                    if(self.selectedSampleRelatedSamplingActivity() == self.samplingActivityResourceId)
                                    {
                                        self.removeFeatureFromCanvas(self.selectedFeature());
                                        self.drawFeatures([selectedFeature]);
                                    } else {
                                        self.highlightAnnotation()
                                    }
                                } 
                            }
                        },
                    })
                },
                buildAnnotationNodes: function(json) {
                    editNodeActiveState = ko.observable(true);
                    nonEditNodeActiveState = ko.observable(true);
                    editNodeActiveState.subscribe(function(active){
                        if (!active) {
                            self.resetSampleLocationTile();
                            updateAnnotations();
                        }
                    });
                    var updateAnnotations = function() {
                        let sampleAnnotations = ko.observableArray();
                        let analysisAreaAnnotations = ko.observableArray();
                        var canvas = self.canvas();
                        if (canvas) {
                            window.fetch(arches.urls.iiifannotations + '?canvas=' + canvas + '&nodeid=' + partIdentifierAssignmentPolygonIdentifierNodeId)
                                .then(function(response) {
                                    return response.json();
                                })
                                .then(function(json) {
                                    json.features.forEach(function(feature) {
                                        feature.properties.graphName = "Physical Thing";
                                        if (self.analysisAreaTileIds.includes(feature.properties.tileId)) {
                                            feature.properties.type = 'analysis_area';
                                            feature.properties.color = '#999999';
                                            feature.properties.fillColor = '#999999';
                                            analysisAreaAnnotations.push(feature);
                                        } else {
                                            feature.properties.type = 'sample_location';
                                            sampleAnnotations.push(feature);
                                        }
                                    });
                                    self.annotationNodes([
                                        {
                                            name: "Sample Locations",
                                            icon: "fa fa-eyedropper",
                                            active: editNodeActiveState,
                                            opacity: ko.observable(100),
                                            annotations: sampleAnnotations
                                        },
                                        {
                                            name: "Analysis Areas",
                                            icon: "fa fa-eye",
                                            active: nonEditNodeActiveState,
                                            opacity: ko.observable(100),
                                            annotations: analysisAreaAnnotations
                                        },
                                    ])
                                    self.highlightAnnotation();
                                });
                        }
                    };
                    self.canvas.subscribe(updateAnnotations);
                    updateAnnotations();
                }
            }]);

            /* overwrites iiif-annotation method */ 
            self.updateTiles = function() {
                _.each(self.featureLookup, function(value) {
                    value.selectedTool(null);
                });

                var partIdentifierAssignmentPolygonIdentifierNodeId = "97c30c42-8594-11ea-97eb-acde48001122";  // Part Identifier Assignment_Polygon Identifier (E42)

                var tileFeatures = ko.toJS(self.tile.data[partIdentifierAssignmentPolygonIdentifierNodeId].features);

                if (tileFeatures) {
                    var featuresNotInTile = self.drawFeatures().filter(function(drawFeature) {
                        return !tileFeatures.find(function(tileFeature) {
                            return tileFeature.id === drawFeature.id;
                        });
                    });

                    self.drawFeatures().forEach(function(drawFeature) {
                        var editedFeatureIndex = tileFeatures.findIndex(function(feature) {
                            return feature.id === drawFeature.id;
                        });

                        if (editedFeatureIndex > -1) {
                            tileFeatures[editedFeatureIndex] = drawFeature;
                        }
                    });

                    self.tile.data[partIdentifierAssignmentPolygonIdentifierNodeId].features([...tileFeatures, ...featuresNotInTile]);
                }
                else {
                    self.widgets.forEach(function(widget) {
                        var id = ko.unwrap(widget.node_id);
                        var features = [];
                        self.drawFeatures().forEach(function(feature){
                            if (feature.properties.nodeId === id) {
                                features.push(feature);
                            }
                        });
                        if (ko.isObservable(self.tile.data[id])) {
                            self.tile.data[id]({
                                type: 'FeatureCollection',
                                features: features
                            });
                        } 
                        else {
                            self.tile.data[id].features(features);
                        }
                    });
                }
            };

                        
            self.deleteFeature = function(feature) {
                /* BEGIN update table */ 
                var partIdentifierAssignmentPolygonIdentifierNodeId = "97c30c42-8594-11ea-97eb-acde48001122";  // Part Identifier Assignment_Polygon Identifier (E42)

                var selectedSampleLocationInstance = self.selectedSampleLocationInstance();
                var selectedSampleLocationInstanceFeaturesNode = ko.unwrap(selectedSampleLocationInstance.data[partIdentifierAssignmentPolygonIdentifierNodeId]);

                if (selectedSampleLocationInstanceFeaturesNode) {
                    var updatedSelectedSampleLocationInstanceFeatures = ko.unwrap(selectedSampleLocationInstanceFeaturesNode.features).filter(function(selectedFeature) {
                        return ko.unwrap(selectedFeature.id) !== ko.unwrap(feature.id);
                    });
                    
                    if (ko.isObservable(selectedSampleLocationInstanceFeaturesNode.features)) {
                        selectedSampleLocationInstanceFeaturesNode.features(updatedSelectedSampleLocationInstanceFeatures);
                    }
                    else {
                        selectedSampleLocationInstanceFeaturesNode.features = ko.observableArray(updatedSelectedSampleLocationInstanceFeatures);
                    }

                    selectedSampleLocationInstance.data[partIdentifierAssignmentPolygonIdentifierNodeId] = selectedSampleLocationInstanceFeaturesNode;
                }

                self.selectedSampleLocationInstance(selectedSampleLocationInstance);
                /* END update table */ 

                /* BEGIN update canvas */ 
                self.removeFeatureFromCanvas(feature);

                var drawFeature = self.drawFeatures().find(function(drawFeature) {
                    return ko.unwrap(drawFeature.id) === ko.unwrap(feature.id);
                });

                if (drawFeature) {
                    self.drawFeatures([]);
                }
                /* END update canvas */ 
            }

            self.editFeature = function(feature) {
                self.featureLayers().forEach(function(featureLayer) {
                    if (featureLayer.feature.id === ko.unwrap(feature.id)) {
                        featureLayer.fireEvent('click');
                    }
                });
            };

            self.drawLayer.subscribe(function(drawLayer) {
                drawLayer.getLayers().forEach(function(layer) {
                    layer.editing.enable();
                    layer.setStyle({color: '#BCFE2B', fillColor: '#BCFE2B'});
                });
            });
        };

        this.clearEditedGeometries = function() {
            if (self.tile.tileid && self.selectedFeature()) {
                self.resetSampleLocationTile();
            }
        };

        this.fetchCardFromResourceId = function(resourceId, nodegroupId) {
            return new Promise(function(resolve, _reject) {
                self._fetchCard(resourceId, null, nodegroupId).then(function(data) {
                    resolve(data);
                });
            });
        };

        this.fetchCardFromGraphId = function(graphId, nodegroupId) {
            return new Promise(function(resolve, _reject) {
                self._fetchCard(null, graphId, nodegroupId).then(function(data) {
                    resolve(data);
                });
            });
        };

        this._fetchCard = function(resourceId, graphId, nodegroupId) {
            return new Promise(function(resolve, _reject) {
                $.getJSON( arches.urls.api_card + ( resourceId || graphId ) ).then(function(data) {
                    var cardData = data.cards.find(function(card) {
                        return card.nodegroup_id === nodegroupId;
                    });

                    var handlers = {
                        'after-update': [],
                        'tile-reset': []
                    };
        
                    var graphModel = new GraphModel({
                        data: {
                            nodes: data.nodes,
                            nodegroups: data.nodegroups,
                            edges: []
                        },
                        datatypes: data.datatypes
                    });

                    resolve(new CardViewModel({
                        card: cardData,
                        graphModel: graphModel,
                        tile: null,
                        resourceId: ko.observable(ko.unwrap(resourceId)),
                        displayname: ko.observable(data.displayname),
                        handlers: handlers,
                        cards: data.cards,
                        tiles: data.tiles,
                        cardwidgets: data.cardwidgets,
                        userisreviewer: data.userisreviewer,
                    }));

                });
            });
        };

        ko.bindingHandlers.scrollTo = {
            update: function (element, valueAccessor) {
                var _value = valueAccessor();
                var _valueUnwrapped = ko.unwrap(_value);
                if (_valueUnwrapped) {
                    element.scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});
                }
            }
        };

        this.initialize();
    };

    ko.components.register('sample-taking-sample-location-step', {
        viewModel: viewModel,
        template: { require: 'text!templates/views/components/workflows/sample-taking-workflow/sample-taking-sample-location-step.htm' }
    });
    return viewModel;
});
