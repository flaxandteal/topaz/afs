define([
    'arches',
    'uuid',
    'knockout',
    'viewmodels/card',
], function(arches, uuid, ko) {
   
    function viewModel(params) {
        var self = this;
        const nameNodeGroupId = 'd2fdae3d-ca7a-11e9-ad84-a4d18cec433a';
        const typeNodeGroupId = '09c1778a-ca7b-11e9-860b-a4d18cec433a';
        const projectNodeGroupId = 'd2fdae3d-ca7a-11e9-ad84-a4d18cec433a';

        const getProp = function(key, prop) {
            if (ko.unwrap(params.value) && params.value()[key]) {
                return prop ? params.value()[key][prop] : params.value()[key];
            } else {
                return null;
            } 
        };

        let typeTileId = getProp('type', 'tileid');
        let nameTileId = getProp('name', 'tileid');
        let projectTileId = getProp('name', 'tileid');

        this.projectResourceId = ko.observable(getProp('projectResourceId'));
        this.typeValue = ko.observable(getProp('type', 'value'));
        this.nameValue = ko.observable(getProp('name', 'value'));
        this.projectValue = ko.observable(getProp('project', 'value'));

        const snapshot = {
            typeValue: self.typeValue(),
            nameValue: self.nameValue(),
            projectValue: self.projectValue(),
        };

        this.updatedValue = ko.pureComputed(function(){
            return {
                projectResourceId: self.projectResourceId(),
                name: {value: self.nameValue(), tileid: nameTileId},
                type: {value: self.typeValue(), tileid: typeTileId},
                project: {value: self.projectValue(), tileid: projectTileId},
            };
        });

        this.updatedValue.subscribe(function(val){
            params.value(val);
        });

        this.buildTile = function(data, nodeGroupId, resourceid, tileid) {
            let res = {
                "tileid": tileid || "",
                "nodegroup_id": nodeGroupId,
                "parenttile_id": null,
                "resourceinstance_id": resourceid,
                "sortorder": 0,
                "tiles": {},
                "data": {},
                "transaction_id": params.form.workflowId
            };
            res.data = data;

            return res;
        };

        this.saveNodeValue = function(nodeid, data, resourceinstanceid, tileid) {
            return $.ajax({
                url: arches.urls.api_node_value,
                type: 'POST',
                data: {
                    'nodeid': nodeid,
                    'data': data,
                    'resourceinstanceid': resourceinstanceid,
                    'tileid': tileid,
                    'transaction_id': params.form.workflowId
                }
            });
        };

        this.saveTile = function(data, nodeGroupId, resourceid, tileid) {
            let tile = self.buildTile(data, nodeGroupId, resourceid, tileid);
            return window.fetch(arches.urls.api_tiles(tileid || uuid.generate()), {
                method: 'POST',
                credentials: 'include',
                body: JSON.stringify(tile),
                headers: {
                    'Content-Type': 'application/json'
                },
            }).then(function(response) {
                if (response.ok) {
                    return response.json();
                }
            });
        };

        params.form.reset = function(){
            self.typeValue(snapshot.typeValue);
            self.nameValue(snapshot.nameValue);
            self.projectValue(snapshot.projectValue);
            // params.form.hasUnsavedData(false);
        };

        params.form.save = function() {
            params.form.complete(false);
            if (!self.nameValue()){
                params.form.error(new Error("Missing Required Value"));
                params.pageVm.alert(new params.form.AlertViewModel('ep-alert-red', "Missing Required Value", "Name is required."));
                return;
            }
            const projectNodeData = self.projectValue().map(pV => {
                return {
                    'resourceId': pV,  // resourceid of the project
                    'ontologyProperty': '',
                    'inverseOntologyProperty':'',
                    'resourceXresourceId':''
                };
            });
    
            const nameTileData = {
                "d2fdc2fa-ca7a-11e9-8ffb-a4d18cec433a": self.nameValue(),
                "d2fdb92b-ca7a-11e9-af41-a4d18cec433a": [
                    "bc35776b-996f-4fc1-bd25-9f6432c1f349"
                ],
                "2e0cd5a4-01ec-11ed-9769-0242ac180008": projectNodeData
                // "0b930757-ca85-11e9-a268-a4d18cec433a": [
                //     "7d069762-bd96-44b8-afc8-4761389105c5"
                // ]
            };

            const typeTileData = {
                "09c1778a-ca7b-11e9-860b-a4d18cec433a": self.typeValue()
            };

            const relatedProjectNodeGroupId = "2e0cd5a4-01ec-11ed-9769-0242ac180008";

            return self.saveTile(nameTileData, nameNodeGroupId, self.projectResourceId(), nameTileId)
                .then(function(data) {
                    nameTileId = data.tileid;
                    self.projectResourceId(data.resourceinstance_id);
                    return self.saveTile(typeTileData, typeNodeGroupId, data.resourceinstance_id, typeTileId);
                })
                // .then(function(data) {
                //     typeTileId = data.tileid;
                //     console.log(data)
                //     return self.saveNodeValue(relatedProjectNodeGroupId, JSON.stringify(data), self.projectResourceId(), projectTileId);
                // })
                .then(function(data) {
                    projectTileId = data.tileid;
                    params.form.savedData(params.form.value());
                    params.form.complete(true);
                    params.form.dirty(false);
                    params.pageVm.alert("");
                });
        };
    }

    ko.components.register('digital-resource-name-step', {
        viewModel: viewModel,
        template: {
            require: 'text!templates/views/components/workflows/create-digital-resource-workflow/digital-resource-name-step.htm'
        }
    });

    return viewModel;
});
