define([
    'knockout',
    'views/components/workflows/summary-step',
], function(ko, SummaryStep) {

    function viewModel(params) {
        var self = this;
        SummaryStep.apply(this, [params]);

        this.resourceLoading = ko.observable(true);
        this.collectionLoading = ko.observable(true);
        this.collectionResourceId = params.resourceid;
        this.collectionData = ko.observableArray();
        this.collectionOfEnvs = ko.observableArray();

        this.getRelatedResources(this.collectionResourceId, this.collectionData);
        // todo tidy up there are no phys things anymore 
        this.collectionData.subscribe(function(val){
            var environmentGraphId = '9519cb4f-b25b-11e9-8c7b-a4d18cec433a';
            val["related_resources"].forEach(function(rr){
                if (rr.graph_id === environmentGraphId) {
                    self.collectionOfEnvs.push({
                        resourceid: rr.resourceinstanceid,
                        name: rr.displayname,
                    });
                }
            });
            this.collectionLoading(false);
            if (!this.resourceLoading()){
                this.loading(false);
            }
        }, this);

        this.resourceData.subscribe(function(val){
            this.provisional = !!val['provisional'];
            this.displayName = val['displayname'] || 'unnamed';
            if (this.provisional) {
                val.resource = val.provisional;
            }
            this.reportVals = {
                DRName: {'name': 'Name', 'value': val.resource['Basic Info']['Name']['@display_value']},
                // DRInterpretation: {'name': 'Interpretation', 'value': val.resource['Description'][0]['Interpretation']['@display_value']},
                DRPurpose: {'name': 'Purpose', 'value': val.resource['Description'][0]['Purpose']['@display_value']},
                DRStatement: {'name': 'Statement', 'value': val.resource['Description'][0]['Statement']['@display_value']},
                // projectName: {'name': 'Project Name', 'value': this.getResourceValue(val.resource['Basic Info'][0]['Name'][0],['Name_content','@display_value'])},
                // DRTimespan: {'name': 'Digital Resource Timespan', 'value': val.resource['Project start']['Date & Time']['@display_value']},
                // DRTeam: {'name': 'Digital Resource Team', 'value': val.resource['Project start']['Group']['@display_value'] || "N/A"},
                // DRLocation: {'name': 'Digital Resource Location', 'value': val.resource['Project start']['Project location']['@display_value'] || "N/A"},
                DRType: {'name': 'Digital Resource File Type', 'value': val.resource['Resource_type']['@display_value']},
                relatedProjectResourceId: {'value': val.resource['Basic Info']['Related project']['instance_details'][0]['resourceId']},
                relatedProject: {'value': this.getResourceValue(val.resource['Basic Info']['Related project'], ['@display_value']) || "N/A"},
                // relatedProjectResourceId: {'value': this.getResourceValue(val.resource['Related project']['instance_details'][0]['resourceId']) || "N/A"},
            };
            this.resourceLoading(false);
            if (!this.collectionLoading()){
                this.loading(false);
            }
        }, this);

    }

    ko.components.register('digital-resource-final-step', {
        viewModel: viewModel,
        template: { require: 'text!templates/views/components/workflows/create-digital-resource-workflow/digital-resource-final-step.htm' }
    });
    return viewModel;
});
