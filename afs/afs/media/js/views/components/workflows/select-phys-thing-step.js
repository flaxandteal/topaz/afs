define([
    'knockout',
    'utils/resource',
    'viewmodels/card',
], function(ko, resourceUtils) {
    
    function viewModel(params) {
        var self = this;
        var componentParams = params.form.componentData.parameters;
        // this.environmentNodeGroupId = 'cc5d6df3-d477-11e9-9f59-a4d18cec433a';
        this.environmentNodeGroupId = 'ae16e448-033f-11ed-91f6-0242ac180008';
        this.physThingTypeNodeId = 'ae16e448-033f-11ed-91f6-0242ac180008';
        // this.environmentPartOfSetNodeId = '63e49254-c444-11e9-afbe-a4d18cec433a';
        // language concept-list environment nodeid 
        this.environmentPartOfSetNodeId = 'b9c1ced7-b497-11e9-a4da-a4d18cec433a';
        this.partNodeGroupId = 'fec59582-8593-11ea-97eb-acde48001122';
        this.partManifestNodeId = '97c30c42-8594-11ea-97eb-acde48001122';
        this.manifestConcepts = [
            '1497d15a-1c3b-4ee9-a259-846bbab012ed', 
            '00d5a7a6-ff2f-4c44-ac85-7a8ab1a6fb70',
            '305c62f0-7e3d-4d52-a210-b451491e6100'
        ]
        this.digitalReferenceTypeNodeId = 'f11e4d60-8d59-11eb-a9c4-faffc265b501';
        this.digitalReferenceNodeGroupId = '8a4ad932-8d59-11eb-a9c4-faffc265b501';
        this.save = params.form.save;
        this.environmentGraphId = componentParams.graphids[0];
        this.projectGraphId = componentParams.graphids[1];
        this.validateThing = componentParams.validateThing;
        this.projectValue = ko.observable();
        this.projectNameValue = ko.observable();
        this.environmentValue = ko.observable();
        this.envThatBelongToProject = ko.observable();
        this.hasSetWithEnvironment = ko.observable();
        this.isEnvironmentValid = ko.observable();
        this.originalValue = params.form.value();
        
        this.updateValues = function(val){
            if (val !== null) {
                self.environmentValue(val.environment);
                self.envThatBelongToProject(val.environmentSet);
                self.projectValue(val.project);
            }
        };

        // update with saved values
        if (params.value()) { 
            this.updateValues(params.value());
        }

        this.locked = params.form.locked;
        
        this.projectValue.subscribe(function(val){
            self.isEnvironmentValid(null);
            self.environmentValue(null);
            if (val) {
                var res = resourceUtils.lookupResourceInstanceData(val);
                // env4 value
                var rid = resourceUtils.lookupResourceInstanceData("129d378f-f95b-4c8b-b30c-a3600d3f1b18");
                console.log("this is val: ", val)
                console.log("this is res: ", res)
                console.log("this is res: ", rid)
                res.then(
                    function(data){
                        self.projectNameValue(data._source.displayname);
                        let setTileResourceInstanceIds;
                        let setTile = data._source.tiles.find(function(tile){
                            return tile.nodegroup_id === self.environmentNodeGroupId;
                        });
                        if (setTile && Object.keys(setTile.data).includes(self.environmentNodeGroupId) && setTile.data[self.environmentNodeGroupId].length) {
                            self.envThatBelongToProject(null);
                            setTileResourceInstanceIds = setTile.data[self.environmentNodeGroupId].map((instance) => instance.resourceId);
                            if (setTileResourceInstanceIds) {
                                self.envThatBelongToProject(setTileResourceInstanceIds);
                            }
                            self.environmentValue(null);
                        } else {
                            self.hasSetWithEnvironment(false);
                        }
                    }
                );
            }
        });

        this.termFilter = ko.pureComputed(function(){
            // I think it's looking for the environment of a set of environments but we're already giving it an environment
            console.log("envs that belong to project", self.envThatBelongToProject)
            if (ko.unwrap(self.envThatBelongToProject)) {
                console.log("envs that belong to project", self.envThatBelongToProject)
                self.hasSetWithEnvironment(true);
                var query = {"op": "and"};
                query[self.environmentPartOfSetNodeId] = {
                    "op": "or",
                    "val":  ko.unwrap(self.envThatBelongToProject)
                };
                return function(term, queryString) {
                    console.log("query: ", query)
                    queryString.set('advanced-search', JSON.stringify([query]));
                    console.log("queryString", queryString)
                    console.log("term", term)
                    if (term) {
                        console.log("term: 1",term)
                        queryString.set('term-filter', JSON.stringify([{"context":"", "id": term,"text": term,"type":"term","value": term,"inverted":false}]));
                    }
                };
            } else {
                return null;
            }
        });

        this.environmentValue.subscribe(async (val) => {
            // if the physical thing value isn't set correctly, return step value
            // to original value
            if (!val) { 
                params.value(self.originalValue); 
                return; 
            }
            const physThing = (await resourceUtils.lookupResourceInstanceData(val))?._source;
            // this is the env
            console.log("phys: ", physThing)
            console.log("val: ", val)
            const digitalReferencesWithManifest = physThing.tiles.
                filter(x => x.nodegroup_id == self.digitalReferenceNodeGroupId &&
                    self.manifestConcepts.includes(x?.data?.[self.digitalReferenceTypeNodeId]));
            const partsWithManifests = physThing.tiles.filter(x => 
                x.nodegroup_id == self.partNodeGroupId &&
                x.data?.[self.partManifestNodeId]?.features?.[0]?.properties?.manifest)

            // Below in defining the 'projectSet' we make sure that we know the collection that the physical thing came from.
            // We need this in order to place child things in the same collection.
            // It remains possible that if the selected physical thing belongs to two different collections
            // within the same selected project we cannot know in which collection we should put it's samples/analysis areas.
            // In this case we are defaulting to the first collection in the project's list of collections that contains the physical thing.
            // this is looking for sets that belong to that env 
            console.log(physThing.tiles.find(x => x.nodegroup_id === self.environmentPartOfSetNodeId))
            const setsThatBelongToTheSelectedThing = physThing.tiles.find(x => x.nodegroup_id === self.environmentPartOfSetNodeId)?.data[self.environmentPartOfSetNodeId].map(y => y.resourceId);
            const projectSet = setsThatBelongToTheSelectedThing.find(setid => self.envThatBelongToProject().includes(setid))
            // questions: What is this workflow suppose to do? 
            const analysisAreaValueId = '31d97bdd-f10f-4a26-958c-69cb5ab69af1';
            const sampleAreaValueId = '7375a6fb-0bfb-4bcf-81a3-6180cdd26123';
            const isArea = physThing.tiles.filter(x =>
                x.nodegroup_id == self.physThingTypeNodeId &&
                (x.data?.[self.physThingTypeNodeId].includes(analysisAreaValueId) ||
                x.data?.[self.physThingTypeNodeId].includes(sampleAreaValueId))
            ).length > 0;

            let canNonDestructiveObservation = false;
            let canDestructiveObservation = false;
            if (params.datasetRoute) {
                canNonDestructiveObservation = (params.datasetRoute == 'non-destructive' && digitalReferencesWithManifest.length && partsWithManifests.length)
                canDestructiveObservation= (params.datasetRoute == 'destructive' && !isArea)
            }

            if(!self.validateThing || canNonDestructiveObservation || canDestructiveObservation){
                params.value({
                    physThingName: physThing.displayname,
                    environment: val,
                    projectSet: projectSet,
                    environmentSet: self.envThatBelongToProject(),
                    project: self.projectValue(),
                    projectName: self.projectNameValue(),
                });
                self.isEnvironmentValid(true);
            } else {
                self.isEnvironmentValid(false);
            }
        });
        
    }

    ko.components.register('select-phys-thing-step', {
        viewModel: viewModel,
        template: {
            require: 'text!templates/views/components/workflows/select-phys-thing-step.htm'
        }
    });

    return viewModel;
});
