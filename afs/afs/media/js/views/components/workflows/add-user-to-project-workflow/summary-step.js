define([
    'jquery',
    'underscore',
    'knockout',
    'arches',
    'views/components/workflows/summary-step',
], function($, _, ko, arches, SummaryStep) {

    function viewModel(params) {
        var self = this
        this.loading = ko.observable(false);
        this.users = ko.observableArray(params.personId);
        this.projectName = ko.observable();
        this.permissions = ko.observable(params.permissions);
        this.projectId = ko.observable(params.projectId['select-project'].value());
        this.resourceUrl = ko.observable(arches.urls.resource);
        this.displayUserNames = ko.observableArray();
        
        const setPermissions = function(){
            return $.ajax({
                url: arches.urls.access_rights,
                type: 'GET',
                headers: {
                    'Content-Type': 'application/json', 
                    'Accept':'application/json'
                },
                data: {
                    projectId: self.projectId,
                    permissions: params.permissions,
                    userIds: params.personId,
                }
            }).done(function(data) {
                console.log(data)
            });
        };

        const getProjectName = function(){
            return $.ajax({
                url: arches.urls.resource_tiles.replace("aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa", self.projectId()),
                type: 'GET',
                headers: {
                    'Content-Type': 'application/json', 
                    'Accept':'application/json'
                },
            }).done(function(data) {
                // takes the name out of the user tiles
                self.projectName(data.tiles[0].display_values[0].value)
            });
        };

        this.users().forEach(element => {
            return $.ajax({
                url: arches.urls.resource_tiles.replace("aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa", element),
                type: 'GET',
                headers: {
                    'Content-Type': 'application/json', 
                    'Accept':'application/json'
                },
            }).done(function(data) {
                // takes the name out of the user tiles
                self.displayUserNames.push(data.tiles[0].display_values[3].value)
            });
        });

        this.setUpDisplayValues = async() => {
            self.loading(true)
            await getProjectName()
            await setPermissions()
            self.loading(false)
        }

        this.setUpDisplayValues()
    }


    ko.components.register('summary-step', {
        viewModel: viewModel,
        template: { require: 'text!templates/views/components/workflows/add-user-to-project-workflow/summary-step.htm' }
    });
    return viewModel;
});
