define([
    'jquery',
    'underscore',
    'knockout',
    'arches',
    'views/components/workflows/summary-step',
], function($, _, ko, arches, SummaryStep) {

    function viewModel(params) {
        var self = this
        this.loading = ko.observable(false);
        this.users = ko.observableArray(params.personId);
        this.projectName = ko.observable();
        this.projectId = ko.observable(params.projectId['select-project'].value());
        this.resourceUrl = ko.observable(arches.urls.resource);
        this.displayUserNames = ko.observableArray();
        
        const setPermissions = function(){
            return $.ajax({
                url: arches.urls.access_rights,
                type: 'DELETE',
                headers: {
                    'Content-Type': 'application/json', 
                    'Accept':'application/json'
                },
                data: {
                    projectId: self.projectId,
                    userIds: params.personId,
                }
            }).done(function(data) {
                console.log(data)
            });
        };

        const getProjectName = function(){
            return $.ajax({
                url: arches.urls.resource_tiles.replace("aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa", self.projectId()),
                type: 'GET',
                headers: {
                    'Content-Type': 'application/json', 
                    'Accept':'application/json'
                },
            }).done(function(data) {
                // takes the name out of the user tiles
                console.log(data.tiles[0].display_values[0].value)
                self.projectName(data.tiles[0].display_values[0].value)
            });
        };

        this.users().forEach(element => {
            return $.ajax({
                url: arches.urls.resource_tiles.replace("aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa", element),
                type: 'GET',
                headers: {
                    'Content-Type': 'application/json', 
                    'Accept':'application/json'
                },
            }).done(function(data) {
                // takes the name out of the user tiles
                const name = data.tiles[0].display_values.filter(t => t.nodeid === '1ed55c78-bfb2-11e9-bea9-a4d18cec433a')[0];
                self.displayUserNames.push(name.value);
            });
        });

        this.setUpDisplayValues = async() => {
            self.loading(true)
            await getProjectName()
            await setPermissions()
            self.loading(false)
        }

        this.setUpDisplayValues()
    }


    ko.components.register('summary-step', {
        viewModel: viewModel,
        template: { require: 'text!templates/views/components/workflows/remove-user-from-project-workflow/summary-step.htm' }
    });
    return viewModel;
});
