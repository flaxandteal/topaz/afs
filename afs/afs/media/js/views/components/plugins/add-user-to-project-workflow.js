define([
    'knockout',
    'jquery',
    'arches',
    'viewmodels/workflow',
    'views/components/workflows/add-user-to-project-workflow/summary-step'

], function(ko, $, arches, Workflow) {
    return ko.components.register('add-user-to-project-workflow', {
        viewModel: function(params) {
            this.componentName = 'add-user-to-project-workflow';
            this.stepConfig = [
                {
                    title: 'Select Project',
                    name: 'select-project',  /* unique to workflow */
                    required: true,
                    informationboxdata: {
                        heading: 'Select Project',
                        text: 'Select a project to update',
                    },
                    layoutSections: [
                        {
                            sectionTitle: 'Select Project',
                            componentConfigs: [
                                {
                                    componentName: 'resource-instance-select-widget',
                                    uniqueInstanceName: 'select-project', /* unique to step */
                                    parameters: {
                                        graphids: [
                                            '0b9235d9-ca85-11e9-9fa2-a4d18cec433a'/* Project */
                                        ],
                                    },
                                },
                            ], 
                        },
                    ],
                },
                {
                    title: 'Add a User to Your Project',
                    name: 'pick-users-step',  /* unique to workflow */
                    required: true,
                    informationboxdata: {
                        heading: 'Update Projects',
                        text: 'Update Project Permissions that Users have ',
                    },
                    layoutSections: [
                        {
                            sectionTitle: 'Select Users',
                            required: true,
                            componentConfigs: [
                                {
                                    componentName: 'resource-instance-multiselect-widget',
                                    required: true,
                                    uniqueInstanceName: 'select-user', /* unique to step */
                                    parameters: {
                                        graphids: [
                                            'f71f7b9c-b25b-11e9-901e-a4d18cec433a'/* Person */
                                        ],
                                    },
                                },
                            ], 
                        },
                        {
                            sectionTitle: 'Access Rights',
                            componentConfigs: [
                                {
                                    componentName: 'access-rights-widget',
                                    tilesManaged: 'none',
                                    uniqueInstanceName: 'select-permissions', /* unique to step */
                                },
                            ], 
                        },
                    ],
                },
                {
                    title: 'Summary',
                    name: 'add-user-to-project-complete',  /* unique to workflow */
                    description: 'Summary',
                    layoutSections: [
                        {
                            componentConfigs: [
                                { 
                                    componentName: 'summary-step',
                                    uniqueInstanceName: 'summary-step',
                                    tilesManaged: 'none',
                                    parameters: {
                                        projectId: "['select-project']",
                                        personId: "['pick-users-step']['select-user']",
                                        permissions: "['pick-users-step']['select-permissions']",
                                    },
                                },
                            ], 
                        },
                    ],
                }
            ];
            Workflow.apply(this, [params]);
            this.quitUrl = arches.urls.plugin('init-workflow');
        },
        template: { require: 'text!templates/views/components/plugins/add-user-to-project-workflow.htm' }
    });
});
