define([
    'knockout',
    'jquery',
    'arches',
    'viewmodels/workflow',
    'viewmodels/workflow-step',
    'views/components/workflows/create-project-workflow/project-name-step',
    'views/components/workflows/create-project-workflow/add-things-step',
    'views/components/workflows/create-project-workflow/create-project-final-step'
], function(ko, $, arches, Workflow) {
    return ko.components.register('create-project-workflow', {
        viewModel: function(params) {
            this.componentName = 'create-project-workflow';

            this.stepConfig = [
                {
                    title: 'Project Name',
                    name: 'set-project-name',  /* unique to workflow */
                    required: true,
                    informationboxdata: {
                        heading: 'Project Name',
                        text: 'Identify the project by giving it a name',
                    },
                    layoutSections: [
                        {
                            componentConfigs: [
                                {
                                    componentName: 'project-name-step',
                                    uniqueInstanceName: 'project-name', /* unique to step */
                                    parameters: {
                                    },
                                },
                            ], 
                        },
                    ]
                },
                {
                    title: 'Project Statement',
                    name: 'set-project-statement',  /* unique to workflow */
                    required: true,
                    informationboxdata: {
                        heading: 'Project Statement',
                        text: 'Indicate the objectives or motivation of the project',
                    },
                    layoutSections: [
                        {
                            componentConfigs: [
                                {
                                    componentName: 'default-card',
                                    uniqueInstanceName: 'project-statement', /* unique to step */
                                    tilesManaged: 'one',
                                    parameters: {
                                        graphid: '0b9235d9-ca85-11e9-9fa2-a4d18cec433a',
                                        nodegroupid: '0b92a414-ca85-11e9-b725-a4d18cec433a',
                                        resourceid: "['set-project-name']['project-name']['projectResourceId']",
                                    },
                                },
                            ], 
                        },
                    ],
                },
                {
                    title: 'Project Start',
                    name: 'set-project-timespan',  /* unique to workflow */
                    required: false,
                    informationboxdata: {
                        heading: 'Project Start',
                        text: 'Indicate the date the project started if known',
                    },
                    layoutSections: [
                        {
                            componentConfigs: [
                                {
                                    componentName: 'default-card',
                                    uniqueInstanceName: 'project-timespan', /* unique to step */
                                    tilesManaged: 'one',
                                    parameters: {
                                        graphid: '0b9235d9-ca85-11e9-9fa2-a4d18cec433a',
                                        nodegroupid: '956d88e8-033f-11ed-91f6-0242ac180008',
                                        resourceid: "['set-project-name']['project-name']['projectResourceId']",
                                        hiddenNodes: ['7ad2df58-0342-11ed-bdc6-0242ac180008'],
                                    },
                                },
                            ], 
                        },
                    ],
                },
                {
                    title: 'Add a related Environment to Your Project',
                    name: 'object-search-step',  /* unique to workflow */
                    workflowstepclass: 'create-project-add-things-step',
                    informationboxdata: {
                        heading: 'Environment',
                        text: 'Add a related Environment to Your Project',
                    },
                    layoutSections: [
                        {
                            componentConfigs: [
                                {
                                    componentName: 'add-things-step',
                                    uniqueInstanceName: 'add-phys-things',
                                    tilesManaged: 'one',
                                    parameters: {
                                        // graphid: '1b210ef3-b25c-11e9-a037-a4d18cec433a', //Collection graph
                                        graphid: '9519cb4f-b25b-11e9-8c7b-a4d18cec433a', //Collection graph
                                        nodegroupid: '466f81d4-c451-11e9-b7c9-a4d18cec433a', //Curation in Collection
                                        nodeid: '466f7dee-c451-11e9-bea9-a4d18cec433a', //Curation_used in Collection (physical thing) curation time
                                        resourceid: "['set-project-name']['project-name']['projectResourceId']",
                                        projectResourceId: "['set-project-name']['project-name']"
                                    },
                                },
                            ],
                        },
                    ],
                },
                {
                    title: 'Summary',
                    name: 'add-project-complete',  /* unique to workflow */
                    description: 'Summary',
                    layoutSections: [
                        {
                            componentConfigs: [
                                { 
                                    componentName: 'create-project-final-step',
                                    uniqueInstanceName: 'create-project-final',
                                    tilesManaged: 'none',
                                    parameters: {
                                        resourceid: "['set-project-name']['project-name']['projectResourceId']",
                                        // collectionResourceId: "['object-search-step']['add-phys-things'][0]['collectionResourceId']", collections are not re-added yet
                                    },
                                },
                            ], 
                        },
                    ],
                }
            ];

            Workflow.apply(this, [params]);
            this.quitUrl = arches.urls.plugin('init-workflow');
        },
        template: { require: 'text!templates/views/components/plugins/create-project-workflow.htm' }
    });
});
