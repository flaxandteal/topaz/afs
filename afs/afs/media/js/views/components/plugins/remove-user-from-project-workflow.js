define([
    'knockout',
    'jquery',
    'arches',
    'viewmodels/workflow',
    'views/components/workflows/remove-user-from-project-workflow/summary-step'

], function(ko, $, arches, Workflow) {
    return ko.components.register('remove-user-from-project-workflow', {
        viewModel: function(params) {
            this.componentName = 'remove-user-from-project-workflow';
            this.stepConfig = [
                {
                    title: 'Select Project',
                    name: 'select-project',  /* unique to workflow */
                    required: true,
                    informationboxdata: {
                        heading: 'Select Project',
                        text: 'Select a project to update',
                    },
                    layoutSections: [
                        {
                            sectionTitle: 'Select Project',
                            componentConfigs: [
                                {
                                    componentName: 'resource-instance-select-widget',
                                    uniqueInstanceName: 'select-project', /* unique to step */
                                    parameters: {
                                        graphids: [
                                            '0b9235d9-ca85-11e9-9fa2-a4d18cec433a'/* Project */
                                        ],
                                    },
                                },
                            ], 
                        },
                    ],
                },
                {
                    title: 'Remove a User from Your Project',
                    name: 'pick-users-step',  /* unique to workflow */
                    required: true,
                    informationboxdata: {
                        heading: 'Update Projects',
                        text: 'Update Project Permissions that Users have ',
                    },
                    layoutSections: [
                        {
                            sectionTitle: 'Select Users',
                            required: true,
                            componentConfigs: [
                                {
                                    componentName: 'resource-instance-multiselect-widget',
                                    required: true,
                                    uniqueInstanceName: 'select-user', /* unique to step */
                                    parameters: {
                                        graphids: [
                                            'f71f7b9c-b25b-11e9-901e-a4d18cec433a'/* Person */
                                        ],
                                    },
                                },
                            ], 
                        },
                    ],
                },
                {
                    title: 'Summary',
                    name: 'remove-user-from-project-complete',  /* unique to workflow */
                    description: 'Summary',
                    layoutSections: [
                        {
                            componentConfigs: [
                                { 
                                    componentName: 'summary-step',
                                    uniqueInstanceName: 'summary-step',
                                    tilesManaged: 'none',
                                    parameters: {
                                        projectId: "['select-project']",
                                        personId: "['pick-users-step']['select-user']",
                                    },
                                },
                            ], 
                        },
                    ],
                }
            ];
            Workflow.apply(this, [params]);
            this.quitUrl = arches.urls.plugin('init-workflow');
        },
        template: { require: 'text!templates/views/components/plugins/remove-user-from-project-workflow.htm' }
    });
});
