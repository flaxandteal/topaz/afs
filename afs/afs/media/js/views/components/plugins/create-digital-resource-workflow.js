define([
    'knockout',
    'jquery',
    'arches',
    'viewmodels/workflow',
    'viewmodels/workflow-step',
    'views/components/workflows/create-digital-resource-workflow/digital-resource-name-step',
    'views/components/workflows/create-digital-resource-workflow/digital-resource-final-step'
], function(ko, $, arches, Workflow) {
    return ko.components.register('create-digital-resource-workflow', {
        viewModel: function(params) {
            this.componentName = 'create-digital-resource-workflow';

            this.stepConfig = [
                {
                    title: 'Digital Resource Name',
                    name: 'set-digital-resource-name',  /* unique to workflow */
                    required: true,
                    informationboxdata: {
                        heading: 'Digital Resource Name',
                        text: 'Identify the digital resource by giving it a name',
                    },
                    layoutSections: [
                        {
                            componentConfigs: [
                                {
                                    componentName: 'digital-resource-name-step',
                                    uniqueInstanceName: 'digital-resources-name', /* unique to step */
                                    parameters: {
                                    },
                                },
                            ], 
                        },
                    ]
                },
                {
                    title: 'Digital Resource Statement',
                    name: 'set-digital-resource-statement',  /* unique to workflow */
                    required: true,
                    informationboxdata: {
                        heading: 'Digital Resource Statement',
                        text: 'Indicate the objectives or motivation of the Digital Resource',
                    },
                    layoutSections: [
                        {
                            componentConfigs: [
                                {
                                    componentName: 'default-card',
                                    uniqueInstanceName: 'digital-resource-statement', /* unique to step */
                                    tilesManaged: 'one',
                                    parameters: {
                                        graphid: '707cbd78-ca7a-11e9-990b-a4d18cec433a',
                                        nodegroupid: 'da1fac57-ca7a-11e9-86a3-a4d18cec433a',
                                        resourceid: "['set-digital-resource-name']['digital-resources-name']['projectResourceId']",
                                    },
                                },
                            ], 
                        },
                    ],
                },
                {
                    title: 'Digital Resource Creation Event',
                    name: 'set-digital-resource-creation-event',  /* unique to workflow */
                    required: true,
                    informationboxdata: {
                        heading: 'Digital Resource Creation Event',
                        text: 'Creation event of digital resource',
                    },
                    layoutSections: [
                        {
                            componentConfigs: [
                                {
                                    componentName: 'default-card',
                                    uniqueInstanceName: 'digital-resource-creation-event', /* unique to step */
                                    tilesManaged: 'one',
                                    parameters: {
                                        graphid: '707cbd78-ca7a-11e9-990b-a4d18cec433a',
                                        nodegroupid: 'de951c11-ca7a-11e9-a778-a4d18cec433a',
                                        resourceid: "['set-digital-resource-name']['digital-resources-name']['projectResourceId']",
                                        hiddenNodes: ['7ad2df58-0342-11ed-bdc6-0242ac180008'],
                                    },
                                },
                            ], 
                        },
                    ],
                },
                {
                    title: 'Upload Files',
                    name: 'upload-files',  /* unique to workflow */
                    required: true,
                    informationboxdata: {
                        heading: 'Add Files',
                        text: 'Add files',
                    },
                    layoutSections: [
                        {
                            componentConfigs: [
                                {
                                    componentName: 'default-card',
                                    uniqueInstanceName: 'files', /* unique to step */
                                    tilesManaged: 'one',
                                    parameters: {
                                        graphid: '707cbd78-ca7a-11e9-990b-a4d18cec433a',
                                        nodegroupid: '563d710e-0f12-11ed-a9df-0242ac120007',
                                        resourceid: "['set-digital-resource-name']['digital-resources-name']['projectResourceId']",
                                        hiddenNodes: ['7ad2df58-0342-11ed-bdc6-0242ac180008'],
                                    },
                                },
                            ], 
                        },
                    ],
                },
                {
                    title: 'Summary',
                    name: 'digital-resource-complete',  /* unique to workflow */
                    description: 'Summary',
                    layoutSections: [
                        {
                            componentConfigs: [
                                { 
                                    componentName: 'digital-resource-final-step',
                                    uniqueInstanceName: 'digital-resource-final',
                                    tilesManaged: 'none',
                                    parameters: {
                                        resourceid: "['set-digital-resource-name']['digital-resources-name']['projectResourceId']",
                                    },
                                },
                            ], 
                        },
                    ],
                }
            ];

            Workflow.apply(this, [params]);
            this.quitUrl = arches.urls.plugin('init-workflow');
        },
        template: { require: 'text!templates/views/components/plugins/create-digital-resource-workflow.htm' }
    });
});
