define([
    'knockout',
    'arches'
], function(ko, arches) {
    var InitWorkflow = function(params) {
        this.loading = ko.observable(false);
        this.workflows = params.workflows.map(function(wf){
            wf.url = arches.urls.plugin(wf.slug);
            return wf;
        }, this);
        this.workflows = ko.observableArray();
        const self = this;
        const getAvailableWorkflows = function() {
            $.ajax({
                url: "/api/available-plugins",
                type: 'GET',
                headers: {
                    'Content-Type': 'application/json', 
                    'Accept':'application/json'
                }
            }).done(function(data) {
                const workflows = params.workflows.filter(function(wf) {
                    console.log(data, wf, data.indexOf(wf.workflowid));
                    return data.indexOf(wf.workflowid) !== -1;
                }).map(function(wf){
                    wf.url = arches.urls.plugin(wf.slug);
                    return wf;
                }, self);
                self.workflows(workflows);
            });
        };
        this.setUpDisplayValues = async() => {
            self.loading(true)
            await getAvailableWorkflows()
            self.loading(false)
        }

        this.setUpDisplayValues()
    };

    return ko.components.register('init-workflow', {
        viewModel: InitWorkflow,
        template: { require: 'text!templates/views/components/plugins/init-workflow.htm' }
    });
});
