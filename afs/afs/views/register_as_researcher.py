import logging
import uuid
from django.contrib.contenttypes.models import ContentType
from guardian.models import GroupObjectPermission, UserObjectPermission
from django.contrib import messages #import messages
from guardian.shortcuts import assign_perm, remove_perm
from typing import Optional
from arches.app.views.search import get_provisional_type
from django.forms import ValidationError
from django.views.generic import View
from arches.app.search.search_engine_factory import SearchEngineFactory
from django.http import HttpResponseNotFound
from django.utils.translation import ugettext as _
from django.shortcuts import render, redirect
from datetime import datetime
from arches.app.search.components.base import SearchFilterFactory
from arches.app.search.elasticsearch_dsl_builder import Bool, Match, Query, Terms, MaxAgg, Aggregation, Nested
from arches.app.utils.permission_backend import get_nodegroups_by_perm
from arches.app.utils.response import JSONResponse
from arches.app.utils.betterJSONSerializer import JSONSerializer, JSONDeserializer
from arches.app.views.base import BaseManagerView
from arches.app.models.system_settings import settings
from arches.app.models.models import Plugin, ResourceInstance
from django.contrib.auth.models import Group
from afs.resource_model_wrappers import Person, ResourceModelWrapper
from arches.app.models.resource import Resource


logger = logging.getLogger(__name__)

class RegisterAsResearcherView(View):
    def get(self, request):
        person: Optional[ResourceModelWrapper]
        try:
            person = Person.find(request.user.username)
        except (ValidationError, ResourceInstance.DoesNotExist):
            person = None
        else:
            logging.warning(person.basic_info_name)
            logging.warning("person.id")

        if request.user.is_authenticated and request.user.username != 'anonymous':
            return render(
                request,
                "register-as-researcher.htm",
                {
                    "main_script": "index",
                    "person": person,
                    "active_page": "Home",
                    "showform": True,
                    "app_title": settings.APP_TITLE,
                    "copyright_text": settings.COPYRIGHT_TEXT,
                    "copyright_year": settings.COPYRIGHT_YEAR,
                    "version": "0.0.0",
                    "show_language_swtich": settings.SHOW_LANGUAGE_SWITCH,
                },
            )
        return render(request, "errors/403.htm")

    def post(self, request):
        try:
            persons = Person.where(identifier=str(request.user.id))
            if persons:
                person = persons[0]
            else:
                person = Person.create(
                    basic_info_name=request.POST["basic_info_name"],
                    statement_description=request.POST["statement_description"],
                    identifier=str(request.user.id)
                )
            for perm in ("view_resourceinstance", "change_resourceinstance"):
                assign_perm(perm, request.user, person.resource)
            remove_perm("no_access_to_resourceinstance", request.user, person.resource)

            person.resource.index()
            request.user.username = person.id
            request.user.save()
            group = Group.objects.get(name="Researcher")
            request.user.groups.add(group)
            group = Group.objects.get(name="Resource Editor")
            request.user.groups.add(group)
            group = Group.objects.get(name="Guest")
            request.user.groups.remove(group)
            request.user.save()

            # We restrict the Guest and Crowdsource User groups so individual
            # user restriction breaks our flow, and is redundant
            ct = ContentType.objects.get(app_label="models", model="resourceinstance")
            objs = UserObjectPermission.objects.filter(content_type=ct, permission__codename="no_access_to_resourceinstance", user=request.user)
            resources = ResourceInstance.objects.filter(pk__in=[uuid.UUID(resourceInstanceId) for resourceInstanceId in objs.values_list("object_pk", flat=True).distinct()])
            for resource in resources:
                remove_perm("no_access_to_resourceinstance", request.user, resource)

            return redirect("plugins/init-workflow")
        except Exception as e:
            logger.exception(e)
            response = {"result": str(e), "message": [_("Request Failed"), _("Unable to save")]}
            messages.error(request, 'That username is not available or is already taken.')
            return redirect("registerasresearcher")