import json
import logging
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.utils.translation import ugettext as _
from django.views.generic import View
from arches.app.models.resource import Resource
from arches.app.models.tile import Tile
from arches.app.utils.betterJSONSerializer import JSONSerializer, JSONDeserializer
from arches.app.utils.response import JSONResponse
from afs.resource_model_wrappers import Project, attempt_well_known_resource_model

logger = logging.getLogger(__name__)


class UpdateResourceListView(View):
    """
    Originally created to support add-things-step in new project workflow
    But it can be used to add/remove a related resource to/from a resource-instance-list nodegroup
    without creating/deleting a new tile

    The request body should look like this
    {
        relatedresourceid: related resourceid to be added or removed,
        nodegroupid : nodegroup_id where the rr_resource is added,
        nodeid : node_id where the rr_resource is added,
        transactionid: transaction_id if a part of workflow,
        data: [{
            resourceid: resourceid of resource to be edited,
            tileid: tileid of resource to be edited if available,
            action: 'add' or 'remove'
        }]
    }
    """

    def post(self, request):

        data = JSONDeserializer().deserialize(request.POST.get("data"))
        nodegroup_id = request.POST.get("nodegroupid", None)
        node_id = request.POST.get("nodeid", None)
        transaction_id = request.POST.get("transactionid", None)
        resource_id = request.POST.get("resourceid", None)

        projects = {str(project.id): project for project in Project.all()}
        resources_to_relate_to_projects = {"add": {}, "remove": {}}

        if not nodegroup_id or not node_id:
            response = {"result": "failed","NODEID": node_id,"nodegroup": nodegroup_id, "message": [_("Request Failed"), _("Unable to save")]}
            return JSONResponse(response, status=400)
            
        try:
            with transaction.atomic():
                for datum in data:
                    action = datum["action"]
                    related_resource_id = datum["resourceid"] if "resourceid" in datum else None
                    tile_id = datum["tileid"] if "tileid" in datum else None

                    related_resource_template = {
                        "resourceId": "",
                        "ontologyProperty": "",
                        "resourceXresourceId": "",
                        "inverseOntologyProperty": "",
                    }

                    if tile_id is not None:
                        tile = Tile.objects.get(pk=tile_id)
                    else:
                        try:
                            tile = Tile.objects.get(resourceinstance=resource_id, nodegroup=nodegroup_id)
                        except ObjectDoesNotExist as e:
                            Resource.objects.get(resourceinstanceid=resource_id)
                            tile = Tile.get_blank_tile(nodeid=node_id, resourceid=resource_id)
                            tile.data[node_id] = []

                    list_of_rr_resources = [data["resourceId"] for data in tile.data[node_id]]

                    if related_resource_id not in list_of_rr_resources and action == "add":
                        related_resource_template["resourceId"] = related_resource_id
                        tile.data[node_id].append(related_resource_template)
                        tile.save(transaction_id=transaction_id)
                    elif related_resource_id in list_of_rr_resources and action == "remove":
                        rr_data = tile.data[node_id]
                        tile.data[node_id] = [rr for rr in rr_data if rr["resourceId"] != related_resource_id]
                        tile.save(transaction_id=transaction_id)

                    if str(resource_id) in projects and str(related_resource_id) not in projects:
                        resources_to_relate_to_projects[action][resource_id] = related_resource_id
                    elif str(resource_id) not in projects and str(related_resource_id) in projects:
                        resources_to_relate_to_projects[action][related_resource_id] = resource_id

        except Exception as e:
            logger.exception(e)
            response = {"result": str(e), "message": [_("Request Failed"), _("Unable to save")]}
            return JSONResponse(response, status=500)

        for project_id, resource_id in resources_to_relate_to_projects["add"].items():
            wkrm = attempt_well_known_resource_model(resource_id)
            logger.warning(resource_id)
            logger.warning(wkrm)
            logger.warning(type(wkrm))
            try:
                logger.warning(type(wkrm.related_project))
                logger.warning(wkrm.related_project)
                wkrm.related_project.append(projects[project_id]).append()
            except AttributeError as e:
                logger.warning(e)
                continue

        for resource_id, resource in resources_to_relate_to_projects["remove"].items():
            wkrm = attempt_well_known_resource_model(resource)
            logger.warning(wkrm)
            try:
                logger.warning(wkrm.related_project)
            except AttributeError:
                continue

        return JSONResponse({"result": "success"}, status=200)

