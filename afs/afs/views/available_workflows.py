from django.views.generic import View
from arches.app.models.models import Plugin
from arches.app.utils.response import JSONResponse
from guardian.shortcuts import get_objects_for_user
from afs.resource_model_hooks import has_any_project

class AvailablePluginsView(View):
    def get(self, request):
        if has_any_project(request.user) or request.user.is_superuser:
            plugins = get_objects_for_user(request.user, "view_plugin", Plugin)
            res = [
                plugin.pluginid
                for plugin in plugins
            ]
        else:
            res = []

        return JSONResponse(res)
