from pickle import GET
from django.http import QueryDict
import logging
import resource
from tokenize import group
from django.views.generic import View
from arches.app.utils.response import JSONResponse
from arches.app.models import models
from arches.app.models.resource import Resource
from arches.app.views.search import search_results
from django.http import HttpRequest
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from guardian.shortcuts import (
    assign_perm,
    remove_perm,
    get_group_perms,
    get_objects_for_group
)
from afs.resource_model_wrappers import Person, ResourceModelWrapper
from django.contrib.auth.models import User
from arches.app.models.resource import Resource
from arches.app.utils.permission_backend import (
    user_can_read_resource,
    user_can_edit_resource,
    user_can_delete_resource,
    user_can_read_concepts,
    user_is_resource_reviewer,
    get_restricted_instances,
    check_resource_instance_permissions,
    get_nodegroups_by_perm,
)


import json

PERMISSIONS = ("readOnly", "readAndWrite")


class AccessRights(View):
    def delete(self, request):
        delete = QueryDict(request.body)
        project_id = delete.get("projectId")
        user_ids = delete.getlist("userIds[]")

        if not user_can_edit_resource(user=request.user, resourceid=project_id):
            return JSONResponse(status=403)

        logging.warn(user_ids)
        group_names = [
            f'{permission}-Project-{project_id}'
            for permission in PERMISSIONS
        ]
        users = [User.objects.get(username=user_id) for user_id in user_ids]
        bl = []
        groups = []
        for user in users:
            for group_name in group_names:
                group, created = Group.objects.get_or_create(name=group_name)
                groups.append(group)

                bl.append(user_can_edit_resource(user=user, resourceid=project_id))
                bl.append(user_can_read_resource(user=user, resourceid=project_id))
                bl.append(user_can_delete_resource(user=user, resourceid=project_id))
                user.groups.remove(group)
            user.save()

        # Update permissions for search
        for group in groups:
            for ri in get_objects_for_group(group, perms=["view_resourceinstance"], klass=models.ResourceInstance):
                logging.warn(ri)
                resource = Resource(ri.resourceinstanceid)
                resource.graph_id = ri.graph_id
                resource.index()

        return JSONResponse({
            "bl": bl,
            # "userId": user_ids,
            # "permissions": permissions,
            # "group_name": group_name,
            # "group": group,
            # "created": created,
            # "groups": users[0].groups.all(),
        }, status=200)


    def get(self, request):
        project_id = request.GET.get("projectId")
        user_ids = request.GET.getlist("userIds[]")
        permissions = request.GET.get("permissions")

        if not user_can_edit_resource(user=request.user, resourceid=project_id):
            return JSONResponse(status=403)

        group_name = f'{permissions}-Project-{project_id}'
        group, created = Group.objects.get_or_create(name=group_name)

        users = [User.objects.get(username=user_id) for user_id in user_ids]
        bl = []
        for user in users:
            bl.append(user_can_edit_resource(user=user, resourceid=project_id))
            bl.append(user_can_read_resource(user=user, resourceid=project_id))
            bl.append(user_can_delete_resource(user=user, resourceid=project_id))
            user.groups.add(group)

        # Update permissions for search
        for ri in get_objects_for_group(group, perms=["view_resourceinstance"], klass=models.ResourceInstance):
            logging.warn(ri)
            resource = Resource(ri.resourceinstanceid)
            resource.graph_id = ri.graph_id
            resource.index()

        return JSONResponse({
            "bl": bl,
            # "userId": user_ids,
            # "permissions": permissions,
            # "group_name": group_name,
            # "group": group,
            # "created": created,
            # "groups": users[0].groups.all(),
        }, status=200)
    
