"""
Django settings for afs project.
"""

import os
import arches
import inspect

try:
    from arches.settings import *
except ImportError:
    pass

from django.contrib.messages import constants as messages


MESSAGE_TAGS = {
        messages.DEBUG: 'alert-secondary',
        messages.INFO: 'alert-info',
        messages.SUCCESS: 'alert-success',
        messages.WARNING: 'alert-warning',
        messages.ERROR: 'alert-danger',
 }

APP_ROOT = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
STATICFILES_DIRS = (os.path.join(APP_ROOT, "media"),) + STATICFILES_DIRS
STATIC_ROOT = ""

DATATYPE_LOCATIONS.append("afs.datatypes")
FUNCTION_LOCATIONS.append("afs.functions")
SEARCH_COMPONENT_LOCATIONS.append("afs.search_components")
TEMPLATES[0]["DIRS"].append(os.path.join(APP_ROOT, "functions", "templates"))
TEMPLATES[0]["DIRS"].append(os.path.join(APP_ROOT, "widgets", "templates"))
TEMPLATES[0]["DIRS"].insert(0, os.path.join(APP_ROOT, "templates"))

APP_PATHNAME = ""

BYPASS_CARDINALITY_TILE_VALIDATION = False

CANTALOUPE_DIR = os.path.join(APP_ROOT, "uploadedfiles")
CANTALOUPE_HTTP_ENDPOINT = "http://localhost:8182/"

LOCALE_PATHS.append(os.path.join(APP_ROOT, "locale"))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "(hdj_k6s^6*+ve_y9i(&$jo4cj4&jb=ryedo$2jh56bi82ye%*"

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ROOT_URLCONF = "afs.urls"
FILE_UPLOAD_PERMISSIONS = 0o644
# a prefix to append to all elasticsearch indexes, note: must be lower case
ELASTICSEARCH_PREFIX = "afs"

ELASTICSEARCH_CUSTOM_INDEXES = []
# [{
#     'module': 'afs.search_indexes.sample_index.SampleIndex',
#     'name': 'my_new_custom_index' <-- follow ES index naming rules
# }]

DATABASES = {
    "default": {
        "ATOMIC_REQUESTS": False,
        "AUTOCOMMIT": True,
        "CONN_MAX_AGE": 0,
        "ENGINE": "django.contrib.gis.db.backends.postgis",
        "HOST": "localhost",
        "NAME": "afs",
        "OPTIONS": {},
        "PASSWORD": "postgis",
        "PORT": "5432",
        "POSTGIS_TEMPLATE": "template_postgis",
        "TEST": {"CHARSET": None, "COLLATION": None, "MIRROR": None, "NAME": None},
        "TIME_ZONE": None,
        "USER": "postgres",
    }
}

INSTALLED_APPS = (
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.gis",
    "arches",
    "arches.app.models",
    "arches.management",
    "guardian",
    "captcha",
    "revproxy",
    "corsheaders",
    "oauth2_provider",
    "django_celery_results",
    "afs",
    "compressor",
    "storages",
    # "debug_toolbar"
)


MIDDLEWARE = [
    # 'debug_toolbar.middleware.DebugToolbarMiddleware',
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    #'arches.app.utils.middleware.TokenMiddleware',
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "arches.app.utils.middleware.ModifyAuthorizationHeader",
    "oauth2_provider.middleware.OAuth2TokenMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "arches.app.utils.middleware.SetAnonymousUser",
]

ALLOWED_HOSTS = ["10.0.2.2", "localhost"]

SYSTEM_SETTINGS_LOCAL_PATH = os.path.join(APP_ROOT, "system_settings", "System_Settings.json")
WSGI_APPLICATION = "afs.wsgi.application"

RESOURCE_IMPORT_LOG = os.path.join(APP_ROOT, "logs", "resource_import.log")

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {"console": {"format": "%(asctime)s %(name)-12s %(levelname)-8s %(message)s",},},
    "handlers": {
        "file": {
            "level": "WARNING",  # DEBUG, INFO, WARNING, ERROR
            "class": "logging.FileHandler",
            "filename": os.path.join(APP_ROOT, "arches.log"),
            "formatter": "console",
        },
        "console": {
            "level": "WARNING",
            "class": "logging.StreamHandler",
            "formatter": "console",
        },
    },
    "loggers": {
        "arches": {
            "handlers": ["file", "console"],
            "level": "WARNING",
            "propagate": True,
        }
    },
}

# Absolute filesystem path to the directory that will hold user-uploaded files.
MEDIA_ROOT = os.path.join(APP_ROOT)

# Sets default max upload size to 15MB
DATA_UPLOAD_MAX_MEMORY_SIZE = 15728640

# Unique session cookie ensures that logins are treated separately for each app
SESSION_COOKIE_NAME = "afs"

CACHES = {
    "default": {"BACKEND": "django.core.cache.backends.locmem.LocMemCache", "LOCATION": "unique-snowflake"},
    "user_permission": {
        "BACKEND": "django.core.cache.backends.db.DatabaseCache",
        "LOCATION": "user_permission_cache",
    },
}

# Identify the usernames and duration (seconds) for which you want to cache the time wheel
CACHE_BY_USER = {"anonymous": 3600 * 24}

TILE_CACHE_TIMEOUT = 600  # seconds
GRAPH_MODEL_CACHE_TIMEOUT = 3600 * 24 * 30  # seconds * hours * days = ~1mo
USER_GRAPH_PERMITTED_CARDS_TIMEOUT = 3600 * 24 * 30  # seconds * hours * days = ~1mo
USER_GRAPH_CARDWIDGETS_TIMEOUT = 3600 * 24 * 30  # seconds * hours * days = ~1mo

MOBILE_OAUTH_CLIENT_ID = ""  #'9JCibwrWQ4hwuGn5fu2u1oRZSs9V6gK8Vu8hpRC4'
MOBILE_DEFAULT_ONLINE_BASEMAP = {"default": "mapbox://styles/mapbox/streets-v9"}

APP_TITLE = "Topaz"
COPYRIGHT_TEXT = "All Rights Reserved."
COPYRIGHT_YEAR = "2019"

CELERY_BROKER_URL = "amqp://guest:guest@localhost"
CELERY_ACCEPT_CONTENT = ["json"]
CELERY_RESULT_BACKEND = "django-db"  # Use 'django-cache' if you want to use your cache as your backend
CELERY_TASK_SERIALIZER = "json"

EMAIL_USE_TLS = True
EMAIL_HOST = os.getenv('EMAIL_HOST')
EMAIL_HOST_USER = os.getenv('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = os.getenv('EMAIL_HOST_PASSWORD')
EMAIL_PORT = 587
DEFAULT_FROM_EMAIL = os.getenv('DEFAULT_FROM_EMAIL')
FORCE_USER_SIGNUP_EMAIL_AUTHENTICATION = True

ENABLE_CAPTCHA = False
ENABLE_USER_SIGNUP = True

# group to assign users who self sign up via the web ui
USER_SIGNUP_GROUP = 'Crowdsource Editor'

ONTOLOGY_NAMESPACES = {
    "http://purl.org/dc/terms/": "dcterms",
    "http://purl.org/dc/elements/1.1/": "dc",
    "http://schema.org/": "schema",
    "http://www.w3.org/2004/02/skos/core#": "skos",
    "http://www.w3.org/2000/01/rdf-schema#": "rdfs",
    "http://xmlns.com/foaf/0.1/": "foaf",
    "http://www.w3.org/2001/XMLSchema#": "xsd",
    "https://linked.art/ns/terms/": "la",
    "http://www.w3.org/1999/02/22-rdf-syntax-ns#": "rdf",
    "http://www.cidoc-crm.org/cidoc-crm/": "",
    "http://www.ics.forth.gr/isl/CRMdig/": "",
    "http://www.ics.forth.gr/isl/CRMgeo/": "geo",
    "http://www.ics.forth.gr/isl/CRMsci/": "sci",
}


RENDERERS += [
    {
        "name": "fors-reader",
        "title": "ASD Hi Res FieldSpec4",
        "description": "Use for exports from all our ASD High Resolution Field Spectroscopy",
        "id": "88dccb59-14e3-4445-8f1b-07f0470b38bb",
        "iconclass": "fa fa-bar-chart-o",
        "component": "views/components/cards/file-renderers/fors-reader",
        "ext": "txt",
        "type": "text/plain",
        "exclude": "",
    },
    {
        "name": "xrf-reader",
        "title": "HP Spectrometer XRF ASCII Output",
        "description": "Use for exports from all our HP XRF outputs",
        "id": "31be40ae-dbe6-4f41-9c13-1964d7d17042",
        "iconclass": "fa fa-bar-chart-o",
        "component": "views/components/cards/file-renderers/xrf-reader",
        "ext": "txt",
        "type": "text/plain",
        "exclude": "",
    },
    {
        "name": "raman-reader",
        "title": "Raman File Reader",
        "description": "Use for exports from all our HP raman and gas chromatograph spectrometers",
        "id": "94fa1720-6773-4f99-b49b-4ea0926b3933",
        "iconclass": "fa fa-bolt",
        "component": "views/components/cards/file-renderers/raman-reader",
        "ext": "txt",
        "type": "text/plain",   
        "exclude": "",
    },
    {
        "name": "pdbreader",
        "title": "PDB File Reader",
        "description": "",
        "id": "3744d5ec-c3f1-45a1-ab79-a4a141ee4197",
        "iconclass": "fa fa-object-ungroup",
        "component": "views/components/cards/file-renderers/pdbreader",
        "ext": "pdb",
        "type": "",
        "exclude": "",
    },
    {
        "name": "pcdreader",
        "title": "Point Cloud Reader",
        "description": "",
        "id": "e96e84f2-bcb2-4ca4-8793-7568b09d7374",
        "iconclass": "fa fa-cloud",
        "component": "views/components/cards/file-renderers/pcdreader",
        "ext": "pcd",
        "type": "",
        "exclude": "",
    },
    # {
    #     "name": "colladareader",
    #     "id": "3732bdf0-74b1-412f-955a-9ca038e7db31",
    #     "iconclass": "fa fa-spoon",
    #     "component": "views/components/cards/file-renderers/colladareader",
    #     "ext": "dae",
    #     "type": "",
    #    "exclude": [],
    # },
]

DOCKER = False

WELL_KNOWN_RESOURCE_MODELS = [
    dict(
        model_name="Person",
        __str__=lambda ri: ri.basic_info_name,
        graphid="f71f7b9c-b25b-11e9-901e-a4d18cec433a",
        basic_info_name={
            "type": str,
            "nodegroupid": "1ed5519e-bfb2-11e9-b995-a4d18cec433a",
            "nodeid": "1ed55c78-bfb2-11e9-bea9-a4d18cec433a",
        },
        statement_description={
            "type": str,
            "nodegroupid": "2a9fea8c-bfb2-11e9-b057-a4d18cec433a",
            "nodeid": "2aa010fd-bfb2-11e9-a01f-a4d18cec433a",
        },
        identifier={
            "type": str,
            "nodegroupid": "2d4b4c23-bfb2-11e9-ba92-a4d18cec433a",
            "nodeid": "2d4b56cc-bfb2-11e9-bda7-a4d18cec433a",
        },
        related_project={
            "type": "Project",
            "nodegroupid": "1ed5519e-bfb2-11e9-b995-a4d18cec433a",
            "nodeid": "033582cc-0282-11ed-b207-0242ac180008",
        }
    ),
    dict(
        model_name="Project",
        __str__=lambda ri: ri.basic_info_name,
        graphid="0b9235d9-ca85-11e9-9fa2-a4d18cec433a",
        basic_info_name={
            "type": str,
            "nodegroupid": "0b926359-ca85-11e9-ac9c-a4d18cec433a",
            "nodeid": "0b92cf5c-ca85-11e9-95b1-a4d18cec433a",
        },
        basic_info_language={
            "type": str,
            "nodegroupid": "0b926359-ca85-11e9-ac9c-a4d18cec433a",
            "nodeid": "0b92f8cc-ca85-11e9-8c26-a4d18cec433a",
        },
        description_statement={
            "type": str,
            "nodegroupid": "0b92a414-ca85-11e9-b725-a4d18cec433a",
            "nodeid": "0b930a8a-ca85-11e9-a000-a4d18cec433a",
        },
        identifier={
            "type": str,
            "nodegroupid": "0b929f00-ca85-11e9-bfe2-a4d18cec433a",
            "nodeid": "0b92c200-ca85-11e9-ae7c-a4d18cec433a",
        }
    ),
    dict(
        model_name="Spatial Description",
        __str__=lambda ri: ri.basic_info_name,
        graphid="cc8ed633-b25b-11e9-a13a-a4d18cec433a",
        basic_info_name={
            "type": str,
            "nodegroupid": "1be4c387-c071-11e9-ba22-a4d18cec433a",
            "nodeid": "1be4cd57-c071-11e9-a818-a4d18cec433a",
        },
        basic_info_language={
            "type": str,
            "nodegroupid": "1be4c387-c071-11e9-ba22-a4d18cec433a",
            "nodeid": "1be4c80f-c071-11e9-82cd-a4d18cec433a",
        },
        related_project={
            "type": "Project",
            "nodegroupid": "1be4c387-c071-11e9-ba22-a4d18cec433a",
            "nodeid": "c21f2a90-029f-11ed-a2d4-0242ac180008",
        },
    ),
    dict(
        model_name="Digital Resources",
        __str__=lambda ri: ri.basic_info_name,
        graphid="707cbd78-ca7a-11e9-990b-a4d18cec433a",
        basic_info_name={
            "type": str,
            "nodegroupid": "d2fdae3d-ca7a-11e9-ad84-a4d18cec433a",
            "nodeid": "d2fdc2fa-ca7a-11e9-8ffb-a4d18cec433a",
        },
        basic_info_language={
            "type": str,
            "nodegroupid": "d2fdae3d-ca7a-11e9-ad84-a4d18cec433a",
            "nodeid": "d2fdb92b-ca7a-11e9-af41-a4d18cec433a",
        },
        creation_person={
            "type": "Person",
            "nodegroupid": "de951c11-ca7a-11e9-a778-a4d18cec433a",
            "nodeid": "6d4cbeb4-0205-11ed-a5ed-0242ac180008"
        },
        creation_team={
            "type": "Group",
            "nodegroupid": "de951c11-ca7a-11e9-a778-a4d18cec433a",
            "nodeid": "6fb24340-0205-11ed-a5ed-0242ac180008"
        },
        creation_location={
            "type": "SpatialDescription",
            "nodegroupid": "de951c11-ca7a-11e9-a778-a4d18cec433a",
            "nodeid": "77d69eb6-01ee-11ed-b392-0242ac180008"
        },
        related_project={
            "type": "Project",
            "nodegroupid": "d2fdae3d-ca7a-11e9-ad84-a4d18cec433a",
            "nodeid": "2e0cd5a4-01ec-11ed-9769-0242ac180008",
        },
    ),
    dict(
        model_name="Group",
        __str__=lambda ri: ri.basic_info_name,
        graphid="07883c9e-b25c-11e9-975a-a4d18cec433a",
        basic_info_name={
            "type": str,
            "nodegroupid": "12707705-c05e-11e9-8177-a4d18cec433a",
            "nodeid": "127095f5-c05e-11e9-bb57-a4d18cec433a",
        },
        basic_info_language={
            "type": str,
            "nodegroupid": "12707705-c05e-11e9-8177-a4d18cec433a",
            "nodeid": "1be4c80f-c071-11e9-82cd-a4d18cec433a",
        },
        basic_info_location={
            "type": "SpatialDescription",
            "nodegroupid": "12707705-c05e-11e9-8177-a4d18cec433a",
            "nodeid": "12708b91-c05e-11e9-823f-a4d18cec433a",
        },
        related_project={
            "type": "Project",
            "nodegroupid": "12707705-c05e-11e9-8177-a4d18cec433a",
            "nodeid": "37b7936a-027c-11ed-9dee-0242ac180008",
        },
    ),
] + [
    dict(model_name=pair[0], graphid=pair[1], basic_info_name=dict(nodegroupid=pair[2][1], nodeid=pair[2][0]), related_project=dict(type="Project", nodegroupid=pair[3], nodeid=pair[3])) for pair in [
        ("Modification",           "5bece219-c456-11e9-8dcd-a4d18cec433a", ("a74aff61-c456-11e9-b253-a4d18cec433a", "a74aef21-c456-11e9-a21f-a4d18cec433a"), "3fcd820c-0331-11ed-98d1-0242ac180008"),
        ("Observation",            "615b11ee-c457-11e9-910c-a4d18cec433a", ("87e40cc5-c457-11e9-8933-a4d18cec433a", "87e3d6a1-c457-11e9-9ec9-a4d18cec433a"), "c4c1fd82-0334-11ed-b230-0242ac180008"),
        ("Environment",            "9519cb4f-b25b-11e9-8c7b-a4d18cec433a", ("b9c1d8a6-b497-11e9-876b-a4d18cec433a", "b9c1ced7-b497-11e9-a4da-a4d18cec433a"), "f68b39b2-026e-11ed-a74c-0242ac180008"),
        ("Textual Work",           "a7b1a7c5-b25b-11e9-8a4e-a4d18cec433a", ("0807626b-c073-11e9-a077-a4d18cec433a", "0807541e-c073-11e9-b0d6-a4d18cec433a"), "bbd1a396-0346-11ed-8898-0242ac180008"),
        ("Digital Resource Group", "553ffdc0-bc02-11ec-9318-025ad0ba9eb1", ("f3e216dc-01b9-11ed-9c9f-0242ac180008", "7a64e700-bc02-11ec-babd-025ad0ba9eb1"), "2bff056e-01d0-11ed-9a67-0242ac180008"),
        ("Collection or Set",      "1b210ef3-b25c-11e9-a037-a4d18cec433a", ("52aa2007-c450-11e9-b5d4-a4d18cec433a", "52aa1673-c450-11e9-8640-a4d18cec433a"), "4c6ecd44-0166-11ed-9c98-0242ac180008"),
    ]
]


try:
    from .package_settings import *
except ImportError:
    pass

try:
    from .settings_local import *
except ImportError:
    pass

if DOCKER:
    try:
        from .settings_docker import *
    except ImportError:
        pass

from arches.settings_docker import *

