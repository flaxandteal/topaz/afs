"""
ARCHES - a program developed to inventory and manage immovable cultural heritage.
Copyright (C) 2013 J. Paul Getty Trust and World Monuments Fund

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import argparse
from arches.app.models.models import Plugin, ResourceInstance
from arches.app.models.graph import Graph
from arches.app.models.resource import Resource
from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand
from guardian.shortcuts import assign_perm, get_perms, remove_perm
from afs.understand import load_resources_from_csv
from afs.resource_model_hooks import project_related_model

EDITOR_ALSO_MODELS = [
    "Spatial Description",
    "Modification",
    "Observation",
    "Textual Work",
    "Digital Resources",
]

PROJECT_COORDINATOR_ONLY_MODELS = [
    "Environment",
    "Person",
    "Digital Resource Group",
    "Collection or Set",
    "Group",
    "Project"
]


class Command(BaseCommand):
    """
    Command for importing JSON-LD data into Arches
    """

    def handle(self, *args, **options):
        researcher = Group.objects.get_or_create(name="Researcher")
        guest = Group.objects.get(name="Guest")
        crowdsource_editor = Group.objects.get(name="Crowdsource Editor")
        resource_editor = Group.objects.get(name="Resource Editor")
        init_workflow_plugin = Plugin.objects.get(slug="init-workflow")
        assign_perm("view_plugin", crowdsource_editor, init_workflow_plugin)
        create_digital_resource_workflow_plugin = Plugin.objects.get(slug="create-digital-resource-workflow")
        assign_perm("view_plugin", crowdsource_editor, create_digital_resource_workflow_plugin)

        for model in PROJECT_COORDINATOR_ONLY_MODELS + EDITOR_ALSO_MODELS:
            for group in (guest, crowdsource_editor, resource_editor):
                graph = Graph.objects.get(name=model)
                for nodegroup in graph.get_nodegroups():
                    for perm in get_perms(group, nodegroup):
                        remove_perm(perm, group, nodegroup)
                print(graph)
                resources = Resource.objects.filter(graph_id=graph.graphid)
                for resource in resources:
                    print("NO ACCESS", resource)
                    assign_perm("no_access_to_resourceinstance", group, resource)

        for model in EDITOR_ALSO_MODELS:
            graph = Graph.objects.get(name=model)
            for nodegroup in graph.get_nodegroups():
                assign_perm("read_nodegroup", crowdsource_editor, nodegroup)
                assign_perm("write_nodegroup", crowdsource_editor, nodegroup)
                assign_perm("read_nodegroup", resource_editor, nodegroup)
                assign_perm("write_nodegroup", resource_editor, nodegroup)

        #for model in PROJECT_COORDINATOR_ONLY_MODELS:
        #    graph = Graph.objects.get(name=model)
        #    for nodegroup in graph.get_nodegroups():
        #        assign_perm("no_access_to_nodegroup", crowdsource_editor, nodegroup)
