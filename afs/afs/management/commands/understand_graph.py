"""
ARCHES - a program developed to inventory and manage immovable cultural heritage.
Copyright (C) 2013 J. Paul Getty Trust and World Monuments Fund

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import argparse
from django.core.management.base import BaseCommand
from afs.understand import graph_one, graph_all


class Command(BaseCommand):
    """
    Command for importing JSON-LD data into Arches
    """

    def add_arguments(self, parser):

        parser.add_argument("-g", "--graph", default=None, action="store", dest="graph", help="Graph name to read from")
        parser.add_argument("-ng", "--nodegroups", default=None, action="store_true", dest="nodegroups", help="Whether to use nodegroups")

    def handle(self, *args, graph=None, nodegroups=False, **options):
        if graph:
            graph_one(graph, nodegroups)
        else:
            graph_all()
