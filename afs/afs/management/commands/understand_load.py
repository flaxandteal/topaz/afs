"""
ARCHES - a program developed to inventory and manage immovable cultural heritage.
Copyright (C) 2013 J. Paul Getty Trust and World Monuments Fund

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import argparse
from django.core.management.base import BaseCommand
from arches.app.models.models import Concept
from afs.resource_model_hooks import project_related_model
from afs.understand import load_resources_from_csv


class Command(BaseCommand):
    """
    Command for importing JSON-LD data into Arches
    """

    def add_arguments(self, parser):

        parser.add_argument("-r", "--resource-model", required=True, action="store", dest="resource_model", help="Graph name to read to")
        parser.add_argument("-s", "--source", action="store", dest="source", help="CSV file to read data from")

    def handle(self, *args, resource_model=None, source=None, **options):
        if not resource_model or not source:
            raise RuntimeError("Need resource model (without spaces) and source CSV file")
        load_resources_from_csv(resource_model, source)
