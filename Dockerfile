ARG ARCHES_BASE=arches
FROM $ARCHES_BASE

RUN useradd arches
RUN chgrp arches ../entrypoint.sh && chmod g+rx ../entrypoint.sh
ARG ARCHES_PROJECT
ENV ARCHES_PROJECT $ARCHES_PROJECT
COPY scripts/entrypoint.sh ${WEB_ROOT}/
COPY scripts/reset_db.sh ${WEB_ROOT}/${ARCHES_PROJECT}/
COPY scripts/minio_upload.sh ${WEB_ROOT}/${ARCHES_PROJECT}/
COPY ${ARCHES_PROJECT} ${WEB_ROOT}/${ARCHES_PROJECT}/
RUN . ../ENV/bin/activate \
    && pip install --no-cache-dir "cachetools>=2.0.0,<6.0"
RUN . ../ENV/bin/activate \
    && pip install --no-cache-dir -r ${WEB_ROOT}/${ARCHES_PROJECT}/requirements.txt --no-binary :all:

COPY settings_docker.py ${WEB_ROOT}/arches/arches/

WORKDIR ${WEB_ROOT}/${ARCHES_PROJECT}/${ARCHES_PROJECT}
RUN mkdir -p /static_root && chown -R arches /static_root
RUN yarn install
WORKDIR ${WEB_ROOT}/${ARCHES_PROJECT}
ENTRYPOINT [ "bash", "-c", "../entrypoint.sh" ]
RUN (. ../ENV/bin/activate && python -c "import django; print(django.__version__)")
USER 1000
