from colorama import Fore, Back, Style
import os
import uuid
from tabulate import tabulate
import sys
import django
from dotenv import load_dotenv
from pathlib import Path
import click
sys.path.append(str(Path(__file__).parent / "afs" / "afs"))
sys.path.append(str(Path(__file__).parent / "docker"))
sys.path.append(str(Path(__file__).parent / "afs" / "afs" / "media" / "packages" / "arches" / "docker"))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings_host")

load_dotenv("docker/env_file.env")
load_dotenv("docker/env_file_local.env")
os.environ["PGHOST"] = "localhost"

import settings
from settings_local import *
from settings_docker import *
django.setup()

from afs.afs.understand import graph_one, graph_all

@click.group()
def cli():
    pass

@cli.command()
@click.option("--nodegroups", type=bool, default=False)
@click.argument("graph", default=None, required=False)
def graph(graph, nodegroups):
    if graph:
        graph_one(graph, nodegroups)
    else:
        graph_all()

@cli.command()
@click.option("--graph", type=str, required=True)
@click.argument("csv")
def load(graph, csv):
    print(graph, csv)

if __name__ == "__main__":
    cli()
