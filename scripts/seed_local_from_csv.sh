docker exec -ti arches-for-science-prj_arches_1 /bin/bash -c "
. ../ENV/bin/activate;
echo Project
#python manage.py understand_load -r Project -s test_data_detailed/projects.csv;
echo SpatialDescription
python manage.py understand_load -r SpatialDescription -s test_data_detailed/spatial_descriptions.csv;
echo Group
python manage.py understand_load -r Group -s test_data_detailed/groups.csv;
echo Person
python manage.py understand_load -r Person -s test_data_detailed/people.csv;
echo User
python manage.py understand_load_user -s test_data_detailed/test_users.csv;
"
