#!/bin/bash

namespace=""
while getopts 'n:h:' OPTION; do
  case "$OPTION" in
    n)
        namespace="${OPTARG} "
        echo "Namespace: ${namespace}"
        ;;
    h)
        echo "Resets the database on the cluster. if needed you can supply a namespace for the context you're using"
        echo "-n                 Add a namespace"
        ;;
    ?)
      echo "script usage: $(basename \$0) [-l] [-h] [-a somevalue]" >&2
      exit 1
      ;;
  esac
done
shift "$(($OPTIND -1))"

pod=$(kubectl ${namespace}get pod -l=tier=backend -o name | head -n 1 | sed 's/.*\///g')
echo $pod
cd afs
for csv in $(ls test_data_detailed/*.csv)
do
  kubectl $namespace exec -ti $pod -- mkdir -p /tmp/test_data_detailed
  kubectl $namespace cp $csv $pod:/tmp/$csv
done
