#!/bin/sh

namespace=""
while getopts 'n:h:' OPTION; do
  case "$OPTION" in
    n)
        namespace="-n ${OPTARG} "
        echo "Namespace: ${namespace}"
        ;;
    h)
        echo "Resets the database on the cluster. if needed you can supply a namespace for the context you're using"
        echo "-n                 Add a namespace"
        ;;
    ?)
      echo "script usage: $(basename \$0) [-l] [-h] [-a somevalue]" >&2
      exit 1
      ;;
  esac
done
shift "$(($OPTIND -1))"

./scripts/copy_csv.sh -n "$namespace"

pod=$(kubectl ${namespace}get pod -l=tier=backend -o name | head -n 1 | sed 's/.*\///g')
kubectl $namespace exec -ti $pod -- /bin/bash -c "
../entrypoint.sh setup_arches;
. ../ENV/bin/activate;
python manage.py createcachetable;
python manage.py packages -o load_package -s afs/pkg/ -y;
python manage.py es index_database;
python manage.py initialize_topaz;
python manage.py understand_load -r Person -s /tmp/test_data_detailed/people.csv;
python manage.py understand_load -r Project -s /tmp/test_data_detailed/projects.csv;
python manage.py understand_load -r SpatialDescription -s /tmp/test_data_detailed/spatial_descriptions.csv;
python manage.py understand_load -r Group -s /tmp/test_data_detailed/groups.csv;
python manage.py understand_load_user -s /tmp/test_data_detailed/test_users.csv;
python manage.py es index_database;
"
