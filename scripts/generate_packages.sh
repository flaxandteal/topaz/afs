#!/bin/sh

# installs the nodeenv
pip install nodeenv

nodeenv NENV

# enters the nodeenv
. NENV/bin/activate
echo "***********************"
echo "*** NPM Install Yarn **"
echo "***********************"
npm install -g yarn
cd afs/afs
echo "***********************"
echo "**** Yarn Install *****"
echo "***********************"
yarn install