#!/bin/bash

# Usage: ./minio-upload my-bucket

bucket=$1

host="mo.ev.openindustry.in"
s3_key="AKIAIXJKXKCLRNMAKQB"
s3_secret="njfyu9eij28d8v7yjqn3j4i71810nv"

content_type="application/octet-stream"
date=`date -R`
uniqDateStr=$(date +'%d:%m:%Y_%T')

read -p "Enter path to dir to be uploaded (path must not end with / example: afs/export default is export): " path
    path=${path:-export}
    echo "Path: ${path}"

read -p "Specify an aws storage bucket name (default is fat-tpz-afs): " bucket
    bucket=${bucket:-fat-tpz-afs}
    echo "Bucket name: ${bucket}"

read -p "Specify an aws end point url (default is mo.ev.openindustry.in): " host
    host=${host:-mo.ev.openindustry.in}
    echo "Endpoint: ${host}"

read -p "Enter aws access key id : " s3_secret
    s3_secret=${s3_secret:-AKIAIXJKXKCLRNMAKQB}
    echo "Access key: ${s3_secret}"

read -p "Enter your aws secret access key: " s3_secret 
    s3_secret=${s3_secret:-njfyu9eij28d8v7yjqn3j4i71810nv}

shopt -s globstar
for file in "${path}"/*.json "${path}"/**/*.json; do
    echo $file
    if [[ "$file" =~ \ |\' ]] # if the file has empty spaces 
    then
        # then remove them
        renamedFile=$(echo $file | sed 's/ //g' -) 
        mv "$file" $renamedFile
        echo $file was renamed to $renamedFile
        file=$renamedFile
    fi

    resource=/"${bucket}"/"${uniqDateStr}"/"${file}"
    _signature="PUT\n\n${content_type}\n${date}\n${resource}"
    signature=`echo -en ${_signature} | openssl sha1 -hmac ${s3_secret} -binary | base64`
    curl -X PUT -T "${file}" \
            -H "Host: ${host}" \
            -H "Date: ${date}" \
            -H "Content-Type: ${content_type}" \
            -H "Authorization: AWS ${s3_key}:${signature}" \
            "https://${host}${resource}"
    echo "${file} has been uploaded" 
done