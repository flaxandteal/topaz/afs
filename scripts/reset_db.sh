#!/bin/bash

docker rm -f ${ARCHES_FOLDER:-afs}_db_1
docker volume rm ${ARCHES_FOLDER:-afs}_postgres-data
docker-compose --env-file docker/env_file.env run --entrypoint /bin/bash arches -c "../entrypoint.sh setup_arches"
docker-compose --env-file docker/env_file.env run --entrypoint /bin/sh arches -c "
. ../ENV/bin/activate; python manage.py createcachetable;
python manage.py packages -o load_package -s afs/pkg/ -y;
python manage.py es index_database
python manage.py initialize_topaz;
python manage.py understand_load -r Project -s test_data/projects.csv;
python manage.py understand_load -r SpatialDescription -s test_data/spatial_descriptions.csv;
python manage.py understand_load -r DigitalResources -s test_data/digital_resources.csv;
python manage.py understand_load -r Group -s test_data/groups.csv;
python manage.py understand_load_user -s test_data/test_users.csv;
python manage.py es index_database;
";
