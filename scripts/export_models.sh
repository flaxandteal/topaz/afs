#!/bin/bash

while true; do
    read -p 'Export models locally or on the cluster?(l or c): ' lc
    case $lc in
        [Ll]* ) echo "locally"; break;;
        [Cc]* ) echo "on the cluster"; break;;
        * ) echo "Please answer l(locally) or c(cluster).";;
    esac
done

if [ "$lc" = "c" ];
then
    read -p 'Pod: ' pod
    read -p 'Namespace: ' namespace
    if [ -z "$pod" ] || [ -z "$namespace" ];
    then 
        echo 'Inputs cannot be blank please try again' 
        exit 0 
    fi
    kubectl -n "${namespace}" exec -ti "${pod}" -- bash -c "mkdir -p afs/export/graphs"
    kubectl -n "${namespace}" exec -ti "${pod}" -- bash -c "mkdir -p afs/export/business_data"
    echo "kubectl -n ${namespace} exec -ti ${pod} -- bash -c "'". ../ENV/bin/activate; python manage.py packages -o export_graphs -d afs/export/graphs -y"'
    kubectl -n "${namespace}" exec -ti "${pod}" -- bash -c ". ../ENV/bin/activate; python manage.py packages -o export_graphs -d afs/export/graphs -y"
    echo "kubectl -n ${namespace} exec -ti ${pod} -- bash -c "'". ../ENV/bin/activate; python manage.py packages -o export_business_data -f json -d afs/export/business_data -y"'
    kubectl -n "${namespace}" exec -ti "${pod}" -- bash -c ". ../ENV/bin/activate; python manage.py packages -o export_business_data -f json -d afs/export/business_data -y"
    echo "kubectl -n ${namespace} exec -ti ${pod} -- bash -c "'"bash minio_upload.sh ${namespace}/export"'
    kubectl -n "${namespace}" exec -ti "${pod}" -- bash -c "bash minio_upload.sh ${namespace}/export"
fi

if [ "$lc" = "l" ];
then
    read -p "Specify an export path (default is afs/export): " export_path
    export_path=${export_path:-afs/export/}
    echo "Path is: ${export_path}"

    mkdir -p ${export_path}/graphs
    mkdir -p ${export_path}/business_data
    docker exec -ti afs_arches_1 /bin/sh -c ". ../ENV/bin/activate; python manage.py packages -o export_graphs -d afs/export/graphs"
    docker exec -ti afs_arches_1 /bin/sh -c ". ../ENV/bin/activate; python manage.py packages -o export_business_data -f json -d afs/export/business_data"
    bash scripts/minio_upload.sh fat-tpz-afs/export
fi